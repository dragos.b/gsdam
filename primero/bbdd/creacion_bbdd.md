***
|Dragos C. Butnariu | Bases de Datos |
|:---|---:| 
| Practica 1 Creación de una Base de Datos | 20/11/2019 |
***

# Creación Base de Datos

## Enunciado

Una nueva empresa nos ha contratado para crear su nueva base de datos de empleados, trabajos, y herramientas. Nos comentan lo siguiente:

Necesitamos una BBDD donde guardar información sobre nuestros empleados que tienen un nombre y pueden tener varios telefonos, a su vez los empleados pueden ser, directores, jefes y trabajadores normales. 

Los directores tienen un cargo, los jefes pertenecen a un departamento y los trabajadores normales tienen una ocupación. Los empleados pueden mandar a otros a hacer trabajos o ser mandados por otro empleado, los trabajos se hacen en un lugar unico y son también de tipo unico, nos interesa conocer el tiempo que un empleado ha tardado en relizar un trabajo. Los trabajos siempre los hace algun empleado como minimo y no hay un máximo, y un empleado realiza solo un trabajo a la vez. Los trabajos se realizan utilizando herramientas, que tienen un nombre *(tenemos herramientas con nombres repetidos, aunque sean diferentes)* y estas son de un tipo determinado *(taladros, radiales, etc.)*, si no existe un trabajo es obvio que no existen herramientas.

Cuando realizamos trabajos utilizamos siempre alguna herramienta, y las herramientas podemos utilizarlas o no en un trabajo, pero solo podremos utilizarla en un trabajo ya que son unicas.


## Diagrama MER

![MER](./creacion_bbdd.png)





*MER*                   
       
##    Modelo Relacional         

(Las claves principales tienen este ***estilo*** ya que Markdown no permite el subrayado)          



| Entidad | Atributos |
|:---|:---|
| EMPLEADO | (***id*** , nombre) |
|TELEFONO | (***idEMPLEADO***, número) |
| DIRECTOR | (***idEMPLEADO***, cargo) |
| JEFE | (***idEMPLEADO***, departamento) |
| TRABAJADOR | (***idEMPLEADO***, ocupacion) |
| TRABAJO | (***tipo***, ***lugar***) |
| HERRAMIENTA | (***id***, nombre, tipo, ***tipoTRABAJO, lugarTRABAJO***)|
| usa | (***idHERRAMIENTA, tipoTRABAJO, lugarTRABAJO***) |
| hace | (***idEMPLEADO, tipoTRABAJO, lugarTRABAJO***, duracion) |
| manda | (***idEMPLEADO_jefe, idEMPLEADO_trabajador***) 

              
## Modelo Optimizado



| Entidad | Atributos |
|:---|:---|
| EMPLEADO | (***id*** , nombre, ***tipoTRABAJO, lugarTRABAJO***, duración, ***idEMPLEADO_jefe***) |
| TELEFONO|***idEMPLEADO***, número |
| DIRECTOR | (***idEMPLEADO***, cargo) |
| JEFE | (***idEMPLEADO***, departamento) |
| TRABAJADOR | (***idEMPLEADO***, ocupacion) |
| TRABAJO | (***tipo***, ***lugar***) |
| HERRAMIENTA | (***id***, nombre, tipo, ***tipoTRABAJO, lugarTRABJAO***)|

## Generación de Tablas

#### `EMPLEADO`

| ***id*** | nombre | ***tipoTRABAJO*** | ***lugarTRABAJO*** | duración | ***idEMPLEADO_jefe*** |
|:--- |:--- |:--- |:--- |:--- |:--- |
| 1 | Carlos | Negocios |Oficina|8 |*null* | 
| 2 | Lázaro | Contabilidad|Oficina |8 | 1 |
| 3 | Laura | Contabilidad|Oficina |8 | 2|
| 4 | Carmen | RRHH |Oficina |8 | 1|
|5 | Monica| Ayudante Contabilidad | Oficina |8 | 3|
|6 |José| Gestión Equipo |Oficina|8| 1|
|7 |Ernesto |Electricidad|C/ Domingo Savio 21, Madrid | 6| 6|
|8 | Vicente |Carpinteria |C/de Pueblanueva 10 |4 | 6|


#### `TELEFONO`

|***idEMPLEADO*** |número |
|--- |--- |
|1|655693321 612248774 |
|2| 633329987 633321594 633297001 |
|3| 620159874|
|4| 632230845|
|5| 695987327 611953258 |
|6| 644521194 |
|7|644982013 |
|8| 611284642|



#### `TRABAJO`

|***tipo*** |***lugar*** |
|---|---|
|Adminsitración |Oficina |
|RRHH|Oficina |
| Contabilidad| Oficina |
|Ayudante Contabilidad| Oficina |
|Gestión Equipo| Oficina |
|Negocios|Oficina |
|Electricidad|C/Domingo Savio 21, Madrid|
|Carpinteria|C/de la Pueblanueva 10|
| Negocios|La Caja Mágica, Madrid |



#### `HERRAMIENTA`

|***id*** |nombre | tipo |***tipoTRABAJO*** |***lugarTRABAJO*** |
|---|---|---|---|---|
|1 |DeWalt |Taladro |Electricidad| C/Domingo Savio 21, Madrid| 
|2|Bosch | Radial|Electricidad|C/Domingo Savio 21, Madrid|
|3 |Black&Decker |Taladro |Carpinteria|C/ Pueblanueva 10, Madrid| 
|4 |Odoo HR | Software RRHH|RRHH|Oficina | 
|5 |Odoo Projects |Software de Proyectos | Gestión de Equipo | Oficina | 


#### `DIRECTOR`

|***idEMPLEADO*** |cargo |
|---|---|
|1 |CEO |
|2 | CFO|


#### `JEFE`

|***idEMPLEADO*** |departamento |
|---|---|
|3 | Administración |
|4 | RRHH|
|6| Jefe Equipo |


#### `TRABAJADOR`	

| ***idEMPLEADO***|ocupación |
|---|---|
|5 |Aydante Adminsitración |
|7 | Electricidad |
|8| Mantenimiento |




## Conclusión

Tras rellenar las tablas con datos *reales* he observado que aumentaría el tamaño de la BBDD de forma muy rapida, ya que se apunta todo lo que se hace, es decir que en un año  habría que pensar que cada cierto tiempo se ejecute alguna herramienta software para crear copias de seguridad, también he de decir que hacer el ejercicio de manera individual me ha servido para aprender bastante más de los tres modelos que estamos haciendo, aunque sigo con el dilema de entender los enunciados, que es de lo más importante, pienso yo, y si lo entiendo bien todo lo demás sabría hacerlo y saldría todo muy rapido y de manera sencilla.





