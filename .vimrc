set nu
set cindent
set sw=4
set expandtab
set softtabstop=2
syntax on

source ~/.vim/vundle.vim

set t_Co=256
set syntax=on
colorscheme desert

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:airline#extensions#tabline#enabled = 1

