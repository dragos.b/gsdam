# Teoría Tema 1 Usabilidad

## Diseño de GUI

Los **principios básicos** a la hora de **diseñar** una GUI son los siguientes:

1. **Sencilla:** que sea una interfaz minimalista con lo minimo necasirio.
1. **Clara:** que siga una organización lógica.
1. **Predecible:** que haga lo que se espera, por ejemplo un boton de cerrar que lo cierre.
1. **Flexible:** que funcione para diferentes plataformas, moviles, tablets, en el caso de una web que sea por ejemplo `responsive`.
1. **Consistente:** que siga un formato de texto en cada elemento.
1. **Intuitiva:** evitar confusiones al usuario, por ejemplo estamos acostumbrados que los botones `aceptar` o `siguiente`
se encuentran a la derecha y `cancelar` a la izquierda.
1. **Coherente:** que el contenido y los colores tengan sentido.

## Usabilidad

Es la **capacidad de un software** *(web, app, programa de escritorio)* de ser **comprendido, aprendido, usado y atractivo** para
el usuario final.

## Principios Usabilidad
1. **Facilidad de Aprendizaje**
1. **Facilidad de Uso**
1. **Flexibilidad**
1. **Robustez**


## Beneficios Usabilidad

La **usabilidad** mejora la calidad de vida de los usuarios, **reduciendo** el **estrés**, incrementado la **satisfacción** y **productividad**.

El **estándar** que más común es `ISO/IEC 9126`, el cual define la usabilidad en términos de claridad, facilidad de aprendizaje, la operatividad
y el atractivo.

1. **Reduce Costes** tanto de **aprendizaje** como de **asistencia y ayuda al usuario**
1. **Disminución de tasa de Errores**
1. **Optimización de costes** de `diseño`, `rediseño` y `mantenimiento`
1. **Incrementa** los visitantes a clientes
1. **Aumento de satisfacción** del usuario
1. **Mejora la imagen y prestigio**


## Accesibilidad

`W3C` *(World Wide Web Consortium)* promueve recomendaciones y estándares que aseguran el crecimiento de la Web a largo plazo

Para que las webs sean accesibles han creado las **Pautas de Accesibilidad al Contenido en la Web** `WCAG` con **tres niveles
de conformidad** que se basan en **3 niveles de Prioridad**

1. **A :** Se cumplen todos los puntos de la **Prioridad 1** 
1. **AA :** Se cumplen todos los puntos de la **Prioridad 1 y 2**
1. **AAA :** Se cumplen todos los putos de las 3 Prioridades 

## Ejercicios

#### Investigar en Internet 4 webs cuyas interfaces incumplan la usabilidad por cada uno de los siguientes motivos.

1. **Estructura no acrode con las pautas.**

En esta web podemos ver que no se siguen las pautas de diseño, demasiados colores que no combinan, no existe una estructura organizada con alineación, no es sencillo el uso de la web, hay demasiada información que impacta mucho.

- [Enlace de la Web](https://www.sphere.bc.ca/test/sruniverse.html)

2. **Demasiadas opciones fuera de elementos desplegables.**

En la siguiente web vemos como existen muchas opciones/enlances mal organizadas.

- [Enlance de la Web](http://www.arngren.net/)


3. **Demasiados tipos de fuentes.**

Vemos que esta web tiene demasiados tipos de fuente a parte de la combinación de elementos, colores no cuadra.

- [Enlace de la Web](http://www.pianoartist.com)


4. **Demasiados colores.**

En la siguiente web podemos observar que existen demasiados elementos (`gifs`) y
demasiados colores.

- [Enlance de la Web](http://www.dokimos.org/ajff/)



Como hemos visto las cuatro webs anteriores estan mal diseñadas y fallan en bastantes
pautas de diseño, y por supuesto no son `responsive`.



### A. Investigar en Internet dos páginas Web que posean cada uno de los niveles de conformidad mencionados.

He encontrado las siguientes webs por cada nivel de accesibilidad, y las he analizado con una herramienta online que analiza el tipo de accesibilidad según el nivel que quieras analizar y te genera un informe de los cambios que deberiamos de hacer para que te certifiquen la web.

- [Herramienta Online](https://achecker.ca/checker/)

#### Nivel A

Vemos que la web xataka consta de muy pocos problemas en cuanto a accesibilidad, pero no cumple todos los requisitos para llegar al nivel A.

- [Xataka](https://www.xataka.com/)


La web del **Gobierno** cuenta con el nivel de accesibilidad **WCAG 2.0 A y AA**

- [Web del Gobierno de España](https://www.lamoncloa.gob.es/Paginas/index.aspx)


#### Nivel AA


Al analizar la web de la **Comunidad de Madrid** con el nivel WCAG 2.0 AA solos nos reporta un problema, que se cambie un `id` para que
sea unico. En la misma web podemos ver que nos informan que el nivel de accesibilidad es **WCAG 2.0 AA**

- [Web Comunidad de Madrid](https://www.comunidad.madrid/)


La web de **Castilla la Mancha** nos informa que cuenta con el nivel de accesibilidad **WCAG 1.0 AA**, y al analizar nos informa
de un unico problema la herramienta, que se valide el tipo de documento.

- [Web Castilla la Mancha](https://www.castillalamancha.es/)


#### Nivel AAA

La web del **Ayuntamiento de Madrid** podría certificarse con el nivel de accesibilidad **WCAG 2.0 AAA**

- [Ayuntaminedo Madrid](https://www.madrid.es/portal/site/munimadrid)


La web de **Cataluña** cuenta con el nivel de accesibilidad **WCAG 2.0 AAA**

- [Web de Cataluña](http://www.catalunya.com/)



### B. Investiga en Internet el código HTML necesario para hacer uso de cada uno de los niveles de conformidad.


**Nivel A**: en elementos como por ejemplo `img, input, applet` utilizar el atributo `alt`, para tablas identificar con el elemento correspondiente las cabeceras de las filas y columnas (`td y th`)

**Nivel AA**: Conseguir que los colores de elementos junto a su color de fondo hagan contraste, crear listas numeradas o sin numerar de la manera correcta (`ol, ul, dl`), evitar el refresco autimatico de la web evitando el uso de `http-equiv="refresh"`

**Nivel AAA**: Utilizar el atributo `abbr` para incluir abreviaciones de elementos como por ejemplo `th`, también se incluye un punto como en el nivle doble A del contrastre entre el color del elemento y el fondo.
