package library;

public class Magazine extends Library
{
    private int number;

    // Constructor
    public Magazine ( String code, String title, int year, int number )
    {
        super( code, title, year );
        this.number = number;

    }

    // Metodo toString
    @Override
    public String toString ()
    {
        String str = "Revista: " + "\n\n" + super.toString() + "\tN� revista: " + number + "\n";
        return str;
    }

}
