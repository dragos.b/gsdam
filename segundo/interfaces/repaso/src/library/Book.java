package library;

public class Book extends Library implements Prestable
{
    private Boolean provided;

    // Constructor
    public Book ( String code, String title, int year )
    {
        super( code, title, year );
        provided = false;
    }

    // Metodo toString
    @Override
    public String toString ()
    {
        String prov = provided()? "si": "no";
        String str  = "Libro:\n\n" + super.toString() + "\tPrestado: " + prov;
        return str;
    }

    // Implementaciones de la Interfaz
    @Override
    public void provide ()
    {
        Boolean rv_pr = provided();
        if( rv_pr )
        {
            System.out.println( "Libro " + title + " est� prestado" );
        }else
        {
            System.out.println( "Libro  " + title + " se va a prestar" );
            this.provided = true;
        }
    }

    @Override
    public void giveBack ()
    {
        System.out.println( "Libro devuelto" );
        this.provided = false;
    }

    @Override
    public Boolean provided ()
    {
        return this.provided? true: false;
    }

}
