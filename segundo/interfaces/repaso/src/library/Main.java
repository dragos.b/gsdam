package library;

public class Main
{

    public static void main ( String[] args )
    {
        // Instantiation of the objects Book & Magazine
        Book     book_1     = new Book( "9781492056270", "Learning Java, 5th Edition", 2020 );
        Book     book_2     = new Book( "9781492056812", "Programming C# 8.0", 2019 );

        Magazine magazine_1 = new Magazine( "x", "Revista Byte TI", 2020, 286 );

        System.out.println( book_1.toString() );
        System.out.println( book_2.toString() );
        System.out.println( magazine_1.toString() );

        book_1.provide();
        book_1.provide();
        System.out.println( book_1.toString() );

        book_1.giveBack();
        System.out.println( book_1.toString() );

    }

}
