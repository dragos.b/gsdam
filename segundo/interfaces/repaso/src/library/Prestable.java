package library;

interface Prestable
{

    void provide ();

    void giveBack ();

    Boolean provided ();

}
