package colegio;

public class Directivo extends Persona {

	// Attributes
	private boolean salesiano;
	private String turno;

	// Constructor
	public Directivo(String dNI, String nombre, String apellidos, double salario, boolean salesiano, String turno) {
		super(dNI, nombre, apellidos, salario);

		this.salesiano = salesiano;
		this.turno = turno;
	}

	// Getters & Setters
	public boolean isSalesiano() {
		return salesiano;
	}

	public void setSalesiano(boolean salesiano) {
		this.salesiano = salesiano;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	@Override
	public String toString() {
		return "\nInformación Directivo/a " + super.getNombre() + " " + super.getApellidos() + "\n\tSalesiano: "
				+ conversor(salesiano) + "\n\tTurno: " + turno + super.toString();
	}

}
