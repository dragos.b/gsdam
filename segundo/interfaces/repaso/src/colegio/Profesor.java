package colegio;

public class Profesor extends Persona {

	// Attributes
	private int n_asignaturas;
	private boolean tutor;

	// Constructor
	public Profesor(String dNI, String nombre, String apellidos, double salario, int n_asignaturas, boolean tutor) {
		super(dNI, nombre, apellidos, salario);

		this.n_asignaturas = n_asignaturas;
		this.tutor = tutor;
	}

	// Getters & Setters
	public int getN_asignaturas() {
		return n_asignaturas;
	}

	public void setN_asignaturas(int n_asignaturas) {
		this.n_asignaturas = n_asignaturas;
	}

	public boolean isTutor() {
		return tutor;
	}

	public void setTutor(boolean tutor) {
		this.tutor = tutor;
	}

	@Override
	public String toString() {
		return "\nInformación de Profesor/a " + super.getNombre() + " " + super.getApellidos()
				+ ":\n\tTotal Asignaturas: " + n_asignaturas + "\n\tTutor/a: " + conversor(tutor) + super.toString();
	}
}
