package colegio;

public class Persona {
	// Attributes
	private String DNI, nombre, apellidos;
	private double salario;

	public static String conversor(boolean entrada) {
		return (entrada == true ? "si" : "no");
	}

	// Constructor
	public Persona(String dNI, String nombre, String apellidos, double salario) {
		DNI = dNI;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.salario = salario;
	}

	// Getters & Setters
	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	@Override
	public String toString() {
		return "\n\tDNI: " + DNI + "\n\tNombre Completo: " + nombre + " " + apellidos + "\n\tSalario = " + salario
				+ "� ";
	}

}
