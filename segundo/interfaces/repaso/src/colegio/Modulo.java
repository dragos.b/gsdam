package colegio;

public class Modulo {
	private String nombre;
	private double horas;
	private Profesor profesor;
	private boolean convalida;
	
	// 
	

	// Constructor
	public Modulo(String nombre, double horas, Profesor profesor, boolean convalida) {
		this.nombre = nombre;
		this.horas = horas;
		this.profesor = profesor;
		this.convalida = convalida;
	}

	// Getters & Setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getHoras() {
		return horas;
	}

	public void setHoras(double horas) {
		this.horas = horas;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	public boolean isConvalida() {
		return convalida;
	}

	public void setConvalida(boolean convalida) {
		this.convalida = convalida;
	}

	@Override
	public String toString() {
		return "\n\t    " + nombre + "\n\t\tHoras: " + horas + "\n\t\tProfesor: " + profesor.getNombre() + " "
				+ profesor.getApellidos() + "\n\t\tConvalida: " + Persona.conversor(convalida) + " ";
	}

}
