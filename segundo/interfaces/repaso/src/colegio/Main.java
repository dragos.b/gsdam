package colegio;

public class Main {

	public static void main(String[] args) {

		// Instanciaci�n de los objetos

		// Profesores
		Profesor profesorUno = new Profesor("58266300M", "Jacinto", "Gutierrez Gonzalez", 1340.55, 5, true);
		Profesor profesorDos = new Profesor("45563222L", "Lucia", "Perez Diaz", 1100.35, 2, false);
		Profesor profesorTres = new Profesor("45459222L", "Francisco", "Gonzalez Diaz", 1400.35, 6, true);

		// Adminsitradores
		Administracion adminUno = new Administracion("64837836P", "Alvaro", "Morales Ortiz", 1450.5, "Grado ADE", 5.5);
		Administracion adminDos = new Administracion("36715138P", "Marta", "Torres Cortes", 1210.5, "GS ADE", 1);

		// Directivos
		Directivo directivoUno = new Directivo("29566035H", "Jaime", "Prieto Lozano", 2300.5, false, "ma�ana");
		Directivo directivoDos = new Directivo("93263626Y", "Ram�n", "Mu�oz Perez", 2700.5, true, "ma�ana");

		// Modulos
		Modulo mod1 = new Modulo("Gesti�n Empresarial", 7, profesorUno, false);
		Modulo mod2 = new Modulo("Ingl�s", 3, profesorDos, true);
		Modulo mod3 = new Modulo("Acceso a Datos", 6, profesorTres, false);
		Modulo mod4 = new Modulo("Desarrollo de Interfaces", 7, profesorTres, false);

		// Array de los modulos, esta soluci�n no es la m�s correcta pero de momento no
		// he encontrado otra.
		// He intentado lo siguiente pero no funciona y no he encontrado otra forma.
		// N6_alumno alumno1 = new N6_alumno("64146315M", ... , {mod1, mod2} );
		// FALLO ---------^

		Modulo[] modulos_1_2 = { mod1, mod2 };
		Modulo[] total_modulos = { mod1, mod2, mod3, mod4 };

		// Alumnos
		Alumno alumno1 = new Alumno("64146315M", "Jorge", "Cuenca Leon", "19950314", "Hombre", false, modulos_1_2);
		Alumno alumno2 = new Alumno("57441780Q", "Hector", "Cortes Parejo", "19971225", "Hombre", false, total_modulos);

		// Salida
		System.out.println(profesorUno.toString());
		System.out.println(profesorDos.toString());
		System.out.println(adminUno.toString());
		System.out.println(adminDos.toString());
		System.out.println(directivoUno.toString());
		System.out.println(directivoDos.toString());
		System.out.println(alumno1.toString());
		System.out.println(alumno2.toString());
	}

}
