package news;

import javax.swing.JFrame;
import javax.swing.JPanel;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.awt.Color;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.DropMode;
import java.awt.Dimension;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Desktop;
import java.awt.SystemColor;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Cursor;



public class Window
{

    private JFrame frame;

    public Window ()
    {
        // Common Atributes
        int width = 450, height = 250;

        // Instance of Components
        frame = new JFrame( "Titulares 20Minutos" );
        JPanel  panel           = new JPanel();
        JLabel  labelTitle      = new JLabel( "Titular '20minutos'" );
        JLabel  labelHeadline   = new JLabel( "Click en el bot\u00F3n" );
        JButton btnUpdate       = new JButton( "Actualizar" );
        JButton btnOpenHeadline = new JButton( "Abrir Noticia" );

        // Frame Properties
        frame.setBounds( 100, 100, width, height );
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.getContentPane().setLayout( null );
        frame.setResizable( false );
        frame.setLocationRelativeTo( null );

        // Panel Properties & additions in panel
        panel.setBounds( 0, 0, width, height );
        panel.setLayout( null );

        panel.add( labelTitle );
        panel.add( labelHeadline );
        panel.add( btnUpdate );
        panel.add( btnOpenHeadline );

        // Add components in frame
        frame.getContentPane().add( panel );

        // Label Title Properties
        labelTitle.setBounds( 20, 11, 406, 45 );
        labelTitle.setFont( new Font( "Tahoma", Font.BOLD, 26 ) );
        labelTitle.setForeground( SystemColor.textHighlight );
        labelTitle.setHorizontalAlignment( SwingConstants.CENTER );

        // Label HeadLine Properties
        labelHeadline.setText( "CLICK EN ACTUALIZAR PARA VER ULTIMO TITULAR" );
        labelHeadline.setBounds( 20, 54, 400, 83 );
        labelHeadline.setFont( new Font( "Tahoma", Font.BOLD, 13 ) );
        labelHeadline.setForeground( SystemColor.textText );
        labelHeadline.setBackground( SystemColor.activeCaption );
        labelHeadline.setHorizontalTextPosition( SwingConstants.CENTER );
        labelHeadline.setHorizontalAlignment( SwingConstants.CENTER );
        labelHeadline.setVerticalTextPosition( SwingConstants.TOP );

        // Button Update Properties
        btnUpdate.setBounds( 20, 148, width / 3, height / 8 );
        btnUpdate.setFont( new Font( "Tahoma", Font.BOLD, 17 ) );
        btnUpdate.setForeground( SystemColor.textHighlightText );
        btnUpdate.setBackground( SystemColor.textHighlight );
        btnUpdate.setCursor( Cursor.getPredefinedCursor( Cursor.HAND_CURSOR ) );

        // Button OpenHeadline Properties
        btnOpenHeadline.setBounds( 270, 148, width / 3, height / 8 );
        btnOpenHeadline.setFont( new Font( "Tahoma", Font.BOLD, 17 ) );
        btnOpenHeadline.setForeground( SystemColor.textHighlightText );
        btnOpenHeadline.setBackground( SystemColor.textHighlight );
        btnOpenHeadline.setCursor( Cursor.getPredefinedCursor( Cursor.HAND_CURSOR ) );
        btnOpenHeadline.setEnabled( false );

        // Buttons Actions
        btnUpdate.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                labelHeadline.setText( headline() );
                btnOpenHeadline.setEnabled( true );
            }
        } );

        btnOpenHeadline.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                openNew();
            }
        } );

    }

    // Method to set visible frame from main method
    public void setVisible ( boolean opt )
    {
        frame.setVisible( opt );
    }

    // Internal class methods
    private static String headline ()
    {
        Document doc;
        String   web             = "https://www.20minutos.es/";
        String   headlineElement = "Sin Titular";

        try
        {
            doc             = Jsoup.connect( web ).get();
            // Si no utilizo el metodo 'first' escoge mas titulares ya que tienen el mismo ID
            headlineElement = doc.select( "h1 > a#m83-82-84" ).first().text();
            //                             ^    ^
            //                Elemento Padre    Elemento hijo con id #n....            

        }catch( IOException e )
        {
            System.out.println( e.getMessage() );
        }
        return formatString( headlineElement );
    }

    private static void openNew ()
    {
        Desktop  desk        = Desktop.getDesktop();
        URI      openUrl;
        Document doc;
        String   web         = "https://www.20minutos.es/";
        String   headlineUrl = "";

        try
        {
            doc         = Jsoup.connect( web ).get();
            // Si no utilizo el metodo 'first' escoge mas titulares ya que tienen el mismo ID
            headlineUrl = doc.select( "h1 > a#m83-82-84" ).first().absUrl( "href" );
            //                         ^    ^
            //            Elemento Padre    Elemento hijo con id #n....  

            openUrl     = new URI( headlineUrl );
            desk.browse( openUrl );

        }catch( IOException | URISyntaxException e )
        {
            System.out.println( e.getMessage() );
        }
    }

    private static String formatString ( String text )
    {
        int    size   = text.length();
        String textRv = "<html> ";
        for ( int i = 0; i < size; i++ )
        {
            char car = text.charAt( i );
            textRv += car;

            if( ( i == 50 || i == 51 ) && car == ' ' )
                textRv += "<br/>";

        }
        textRv += "<html/>";
        return textRv;
    }
}
