package ejercicios;

import java.util.Scanner;

public class N5 {

	public static void main(String[] args) {
		int user_num, fact_result;
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter 1 number to calculate his factorial: ");
		user_num = sc.nextInt();

		fact_result = fact(user_num);

		System.out.println("The factorial of " + user_num + "! = " + fact_result);

		sc.close();
	}

	public static int fact(int n) {
		if (n == 1)
			return n;

		return n * fact(n - 1);
	}

}
