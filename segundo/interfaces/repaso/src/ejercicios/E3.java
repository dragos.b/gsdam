package ejercicios;

public class E3 {

	public static void main(String[] args) {
		int[] numbers = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377 };
		int pairs = 0;

		for (int i = 0; i < numbers.length; i++) {
			int index = numbers[i];
			if (index % 2 == 0)
				pairs++;
		}
		System.out.println("The total pairs numbers in array is: " + pairs);

	}

}
