package traductor;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.JTextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;



public class Window
{

    private JFrame     frmTranslate;
    private JTextField textSp;
    private JTextField textEng;

    public Window ()
    {

        // Frame
        frmTranslate = new JFrame( "Traductor" );
        frmTranslate.setIconImage(
                Toolkit.getDefaultToolkit().getImage( Window.class.getResource( "/traductor/language.png" ) ) );
        frmTranslate.getContentPane().setBackground( SystemColor.control );
        frmTranslate.setForeground( SystemColor.inactiveCaption );
        frmTranslate.setBackground( SystemColor.inactiveCaption );
        frmTranslate.getContentPane().setForeground( SystemColor.inactiveCaption );
        frmTranslate.setBounds( 100, 100, 300, 300 );
        frmTranslate.setResizable( false );
        frmTranslate.setLocationRelativeTo( null );
        frmTranslate.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frmTranslate.getContentPane().setLayout( null );

        // Labels
        JLabel labelTitle = new JLabel( "Traductor" );                          // Instancia
        labelTitle.setForeground( SystemColor.textHighlight );                  // Color texto
        labelTitle.setFont( new Font( "Courier New", Font.BOLD, 28 ) );         // Propiedades de la fuente, con un objeto 'Font'
        labelTitle.setHorizontalAlignment( SwingConstants.CENTER );             // Propiedad align
        labelTitle.setBounds( 10, 11, 268, 35 );                                // Propiedad de ubicacion y tama�o

        JLabel labelSp = new JLabel( "Espa\u00F1ol" );
        labelSp.setForeground( SystemColor.textHighlight );
        labelSp.setFont( new Font( "Courier New", Font.BOLD, 17 ) );
        labelSp.setHorizontalAlignment( SwingConstants.CENTER );
        labelSp.setBounds( 10, 57, 268, 35 );

        JLabel laabelEng = new JLabel( "Ingl\u00E9s" );
        laabelEng.setForeground( SystemColor.textHighlight );
        laabelEng.setFont( new Font( "Courier New", Font.BOLD, 17 ) );
        laabelEng.setHorizontalAlignment( SwingConstants.CENTER );
        laabelEng.setBounds( 10, 136, 268, 35 );

        // Text Fields
        textSp = new JTextField();
        textSp.setFont( new Font( "Courier New", Font.BOLD, 17 ) );
        textSp.setHorizontalAlignment( SwingConstants.CENTER );
        textSp.setBounds( 10, 90, 268, 35 );
        textSp.setColumns( 10 );
        textSp.setToolTipText( "Palabra en Espa\u00F1ol a traducir a Ingl\u00E9s" );

        textEng = new JTextField();
        textEng.setForeground( SystemColor.textHighlight );
        textEng.setFont( new Font( "Courier New", Font.BOLD, 17 ) );
        textEng.setHorizontalAlignment( SwingConstants.CENTER );
        textEng.setBounds( 10, 169, 268, 35 );
        textEng.setColumns( 10 );
        textEng.setEditable( false );

        // Button
        JButton btnTranslate = new JButton( "Traducir" );
        btnTranslate.setBackground( SystemColor.textHighlight );
        btnTranslate.setForeground( SystemColor.inactiveCaptionBorder );
        btnTranslate.setFont( new Font( "Courier New", Font.BOLD, 18 ) );
        btnTranslate.setBounds( 10, 215, 268, 35 );

        // Add components in frame
        frmTranslate.getContentPane().add( labelTitle );
        frmTranslate.getContentPane().add( labelSp );
        frmTranslate.getContentPane().add( textSp );
        frmTranslate.getContentPane().add( laabelEng );
        frmTranslate.getContentPane().add( textEng );
        frmTranslate.getContentPane().add( btnTranslate );

        //frmTraductor.setVisible( true );

        // Button Action
        btnTranslate.addActionListener( new ActionListener()
        {
            @Override
            public void actionPerformed ( ActionEvent e )
            {
                translate2Sp( textSp.getText(), textEng );
            }

        } );
    }

    // Lo utilizo en Main ya que la variable frmTranslate est� encapsulada.
    public void setVisible ( boolean opt )
    {
        frmTranslate.setVisible( opt );
    }

    // Metodo privado para utilizar en esta misma clase.
    private static void translate2Sp ( String spanishWord, JTextField textIng )
    {
        Document doc;
        if( spanishWord.length() > 0 )
        {
            String web = "https://www.spanishdict.com/traductor/" + spanishWord;
            String englishWord;

            // Conexi�n a la web y guardamos el html en el objeto 'doc' de tipo Document
            try
            {
                doc = Jsoup.connect( web ).get();
                Elements word = doc.getElementById( "quickdef1-es" ).getElementsByClass( "_1btShz4h" );
                englishWord = word.get( 0 ).html();
                textIng.setText( englishWord );

            }catch( IOException e1 )
            {
                e1.printStackTrace();
            }

        }
    }

}
