package skel;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JScrollBar;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import java.awt.ComponentOrientation;
import javax.swing.UIManager;



public class FrameConPaneles
{

    private JFrame frame;
    JPanel         panel_0 = new JPanel();
    JPanel         panel_1 = new JPanel();
    JPanel         panel_2 = new JPanel();
    JPanel         panel_3 = new JPanel();
    Login          login   = new Login();
    Timer temporizador;
    private JTextField textFieldUsuario;
    private JPasswordField passwordField;

    /**
     * Launch the application.
     */
    public static void main ( String[] args )
    {
        EventQueue.invokeLater( new Runnable()
        {
            public void run ()
            {
                try
                {
                    FrameConPaneles window = new FrameConPaneles();
                    window.frame.setVisible( true );
                }catch( Exception e )
                {
                    e.printStackTrace();
                }
            }
        } );
    }

    /**
     * Create the application.
     */
    public FrameConPaneles ()
    {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize ()
    {
        // Create Main Frame and define the attributes.
        frame = new JFrame();
        frame.setResizable( false );
        frame.setBounds( 100, 100, 788, 539 );
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.setLocationRelativeTo( null );

        // Create all elements
        JMenuBar  menuBar    = new JMenuBar();
        JMenu     unMenu     = new JMenu( "HELP" );
        JMenuItem menuItem   = new JMenuItem( "VIEW HELP" );

        // Panel 1
        JLabel    lbl0       = new JLabel( "PANEL Nº 0" );
        JButton   btn0_toP1  = new JButton( "TO PANEL 1" );

        // Panel 2
        JLabel    lbl1       = new JLabel( "PANEL Nº 1" );
        JButton   btn1_toP0  = new JButton( "TO PANEL 1" );
        JButton   btn1_toP2  = new JButton( "TO PANEL 2" );

        // Panel 3
        JLabel    lbl2       = new JLabel( "PANEL Nº 2" );
        JButton   btn2_toP3  = new JButton( "TO PANEL 3" );
        JButton   btn2_toP0  = new JButton( "TO PANEL 1" );

        // Panel 4
        JLabel    lbl_3      = new JLabel( "PANEL Nº 3" );
        JButton   btn3_toP2  = new JButton( "TO PANEL 2" );
        JButton   btn_3_toP4 = new JButton( "TO PANEL 4" );

        JButton   btnExit    = new JButton( "SALIR" );

        frame.setJMenuBar( menuBar );

        menuBar.add( unMenu );
        unMenu.add( menuItem );
        frame.getContentPane().setLayout( new CardLayout( 0, 0 ) );

        // Panel_0 Attributes
        panel_0.setBackground( Color.CYAN );
        panel_0.setLayout( new BorderLayout( 0, 0 ) );
        panel_0.add( lbl0, BorderLayout.NORTH );
        panel_0.add( btn0_toP1, BorderLayout.SOUTH );

        // Panel_1 Attributes
        panel_1.setBackground( Color.YELLOW );
        panel_1.add( btn1_toP0 );
        panel_1.add( lbl1 );
        panel_1.add( btn1_toP2 );

        // Panel_2 Attributes
        panel_2.setFont( new Font( "SansSerif", Font.PLAIN, 25 ) );
        panel_2.setBackground( Color.MAGENTA );
        panel_2.setLayout( new GridLayout( 0, 1, 0, 0 ) );
        panel_2.add( btn2_toP0 );
        panel_2.add( lbl2 );
        panel_2.add( btn2_toP3 );

        frame.getContentPane().add( panel_0, BorderLayout.NORTH );
        frame.getContentPane().add( panel_1, BorderLayout.SOUTH );
        frame.getContentPane().add( panel_2, BorderLayout.WEST );

        lbl0.setHorizontalAlignment( SwingConstants.CENTER );
        lbl1.setFont( new Font( "SansSerif", Font.PLAIN, 20 ) );
        lbl2.setFont( new Font( "SansSerif", Font.PLAIN, 20 ) );
        lbl2.setHorizontalTextPosition( SwingConstants.CENTER );
        lbl2.setHorizontalAlignment( SwingConstants.CENTER );
        lbl_3.setFont( new Font( "SansSerif", Font.PLAIN, 20 ) );

        btn0_toP1.setBorder( new SoftBevelBorder( BevelBorder.LOWERED, null, null, null, null ) );
        btn0_toP1.setBackground( Color.WHITE );
        btn0_toP1.setAlignmentX( Component.CENTER_ALIGNMENT );

        GridBagLayout gbl_panel_3 = new GridBagLayout();
        gbl_panel_3.columnWidths  = new int[] { 70, 70, 0, 0, 70, 70 };
        gbl_panel_3.rowHeights    = new int[] { 16, 0, 0, 16, 16, 16 };
        gbl_panel_3.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
        gbl_panel_3.rowWeights    = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };

        GridBagConstraints gbc_lbl_3 = new GridBagConstraints();
        gbc_lbl_3.insets = new Insets( 0, 0, 5, 5 );
        gbc_lbl_3.anchor = GridBagConstraints.NORTH;
        gbc_lbl_3.gridx  = 3;
        gbc_lbl_3.gridy  = 2;

        GridBagConstraints gbc_btn3_toP2 = new GridBagConstraints();
        gbc_btn3_toP2.anchor     = GridBagConstraints.SOUTH;
        gbc_btn3_toP2.gridheight = 5;
        gbc_btn3_toP2.insets     = new Insets( 0, 0, 5, 5 );
        gbc_btn3_toP2.gridx      = 1;
        gbc_btn3_toP2.gridy      = 0;

        GridBagConstraints gbc_btnExit = new GridBagConstraints();
        gbc_btnExit.anchor     = GridBagConstraints.SOUTH;
        gbc_btnExit.gridheight = 5;
        gbc_btnExit.insets     = new Insets( 2, 2, 0, 0 );
        gbc_btnExit.gridx      = 4;
        gbc_btnExit.gridy      = 0;

        GridBagConstraints gbc_btn3_toP4 = new GridBagConstraints();
        // btn_3_toP4
        gbc_btn3_toP4.anchor     = GridBagConstraints.CENTER;
        gbc_btn3_toP4.gridheight = 3;
        gbc_btn3_toP4.insets     = new Insets( 4, 4, 0, 0 );
        gbc_btn3_toP4.gridx      = 3;
        gbc_btn3_toP4.gridy      = 2;

        frame.getContentPane().add( panel_3, BorderLayout.EAST );
        panel_3.setBackground( Color.ORANGE );
        panel_3.setLayout( gbl_panel_3 );
        panel_3.add( lbl_3, gbc_lbl_3 );
        panel_3.add( btn3_toP2, gbc_btn3_toP2 );
        panel_3.add( btnExit, gbc_btnExit );
        panel_3.add( btn_3_toP4, gbc_btn3_toP4 );

        JPanel panel_4   = new JPanel();
        JPanel panel_4_1 = new JPanel();
        JPanel panel_4_2 = new JPanel();
        JPanel panel_4_3 = new JPanel();
        JPanel panel_4_4 = new JPanel();

        panel_4_1.setVisible( true );
        panel_4_2.setVisible( false );
        panel_4_3.setVisible( false );
        panel_4_4.setVisible( false );
        JButton btn_4_3 = new JButton( "Panel 3" );
        btn_4_3.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                panel_4_1.setVisible( false );
                panel_4_2.setVisible( false );
                panel_4_3.setVisible( false );
                panel_4_4.setVisible( true );

            }
        } );
        JButton btn_4_4 = new JButton( "Panel 0" );
        btn_4_4.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                panel_4_1.setVisible( true );
                panel_4_2.setVisible( false );
                panel_4_3.setVisible( false );
                panel_4_4.setVisible( false );

            }
        } );

        panel_4_1.setBackground( Color.BLACK );
        panel_4_2.setBackground( Color.BLUE );
        panel_4_3.setBackground( Color.RED );
        panel_4_4.setBackground( Color.YELLOW );

        
        panel_4_1.setLayout(null);
        
        JLabel labelUsuario = new JLabel("Usuario");
        labelUsuario.setHorizontalAlignment(SwingConstants.RIGHT);
        labelUsuario.setBounds(42, 29, 106, 19);
        labelUsuario.setFont(new Font("Tahoma", Font.BOLD, 15));
        labelUsuario.setForeground(Color.WHITE);
        panel_4_1.add(labelUsuario);
        
        textFieldUsuario = new JTextField();
        textFieldUsuario.setBounds(185, 26, 129, 29);
        panel_4_1.add(textFieldUsuario);
        textFieldUsuario.setColumns(10);
        
        JLabel labelContra = new JLabel("Contrase\u00F1a");
        labelContra.setHorizontalAlignment(SwingConstants.RIGHT);
        labelContra.setBounds(42, 76, 106, 19);
        labelContra.setForeground(Color.WHITE);
        labelContra.setFont(new Font("Tahoma", Font.BOLD, 15));
        panel_4_1.add(labelContra);
        
        passwordField = new JPasswordField();
        passwordField.setBounds(185, 73, 129, 29);
        panel_4_1.add(passwordField);
        panel_4_2.setLayout(null);

        panel_4_3.setLayout( new FlowLayout() );
        panel_4_3.add( btn_4_3 );

        panel_4_4.setLayout( new FlowLayout() );
        panel_4_4.add( btn_4_4 );

        GridLayout gl_panel_4 = new GridLayout( 2, 2 );
        panel_4.setLayout( gl_panel_4 );

        frame.getContentPane().add( panel_4, "name_318627737866500" );
        panel_4.add( panel_4_1 );
        
        JButton btnLogin = new JButton("Login");
       
        btnLogin.setFont(new Font("Tahoma", Font.BOLD, 16));
        btnLogin.setBounds(96, 159, 170, 29);
        panel_4_1.add(btnLogin);
        
        JLabel labelIncorrectos = new JLabel("Datos Incorrectos !");
        labelIncorrectos.setHorizontalAlignment(SwingConstants.CENTER);
        labelIncorrectos.setFont(new Font("Tahoma", Font.BOLD, 15));
        labelIncorrectos.setForeground(Color.RED);
        labelIncorrectos.setBounds(96, 124, 170, 14);
        labelIncorrectos.setVisible( false );
        panel_4_1.add(labelIncorrectos);
        
        JProgressBar progressBar = new JProgressBar();
        progressBar.setBounds(96, 214, 170, 19);
        progressBar.setVisible( true );
        progressBar.setValue( 1 );
        progressBar.setMaximum( 100 );

        
        
        
        panel_4.add( panel_4_2 );
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportBorder(UIManager.getBorder("Button.border"));
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setBounds(10, 11, 371, 209);
        panel_4_2.add(scrollPane);
        JButton btn_4_2 = new JButton( "Panel 2" );
        scrollPane.setColumnHeaderView(btn_4_2);
        
        JTextArea txtrLoremIpsumDolor_1 = new JTextArea();
        scrollPane.setViewportView(txtrLoremIpsumDolor_1);
        txtrLoremIpsumDolor_1.setEditable(false);
        txtrLoremIpsumDolor_1.setLineWrap(true);
        txtrLoremIpsumDolor_1.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nunc id cursus metus aliquam eleifend mi in nulla posuere. Posuere morbi leo urna molestie at elementum eu. Nibh tortor id aliquet lectus proin nibh. Eget arcu dictum varius duis at consectetur. Est lorem ipsum dolor sit amet consectetur adipiscing elit pellentesque. Non quam lacus suspendisse faucibus interdum posuere. In nibh mauris cursus mattis molestie a iaculis at erat. Ullamcorper eget nulla facilisi etiam dignissim. Ut diam quam nulla porttitor massa id neque aliquam vestibulum. Ultrices gravida dictum fusce ut placerat. Nunc eget lorem dolor sed viverra ipsum nunc. Lorem sed risus ultricies tristique nulla aliquet enim tortor at. Nunc sed augue lacus viverra vitae congue eu. Amet dictum sit amet justo.");
        txtrLoremIpsumDolor_1.setRows(20);
        btn_4_2.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                panel_4_1.setVisible( false );
                panel_4_2.setVisible( false );
                panel_4_3.setVisible( true );
                panel_4_4.setVisible( false );
            }
        } );
        panel_4.add( panel_4_3 );
        panel_4.add( panel_4_4 );
        panel_4_1.add(progressBar);

        temporizador = new Timer(10, new ActionListener()
        {
            
            @Override
            public void actionPerformed ( ActionEvent e )
            {
            
                int valor = progressBar.getValue();
                if (valor >=100) {
                    temporizador.stop();
                    return;
                }
                progressBar.setValue( ++valor );
                
            }
        });

        
        /*
         * Buttons Action Listeners
         */

        btn0_toP1.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {

                panel_0.setVisible( false );
                panel_1.setVisible( true );
                frame.dispose();
                frame.setUndecorated( true );
                frame.setVisible( true );
            }
        } );

        btn1_toP0.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                frame.dispose();
                frame.setUndecorated( false );
                frame.setVisible( true );

                panel_0.setVisible( true );
                panel_1.setVisible( false );
            }
        } );

        btn1_toP2.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                frame.dispose();
                frame.setUndecorated( false );
                frame.setVisible( true );

                panel_1.setVisible( false );
                panel_2.setVisible( true );
            }
        } );

        btn2_toP0.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                panel_2.setVisible( false );
                panel_0.setVisible( true );
            }
        } );

        btn2_toP3.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                panel_2.setVisible( false );
                panel_3.setVisible( true );
            }
        } );

        btn3_toP2.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                panel_3.setVisible( false );
                panel_2.setVisible( true );
            }
        } );

        btn_3_toP4.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                panel_3.setVisible( false );
                panel_4.setVisible( true );
            }
        } );

        btnExit.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
                frame.dispose(); // Te aseguras que se libere bien la Ram.
                System.exit( 0 );
            }
        } );

        btnLogin.addActionListener( new ActionListener()
        {
            public void actionPerformed ( ActionEvent e )
            {
               temporizador.start();

                try
                {
                    if( login.compruebaDatos( "usuarios.txt", textFieldUsuario.getText(),
                            passwordField.getPassword() ) )
                    {
                        labelIncorrectos.setVisible( false );
                        panel_4_2.setVisible( true );
                        panel_4_1.setVisible( false );
                        textFieldUsuario.setText( "" );
                        passwordField.setText( "" );
                    }else
                    {
                        labelIncorrectos.setVisible( true );
                        textFieldUsuario.setText( "" );
                        passwordField.setText( "" );
                    }
                }catch( IOException e1 )
                {
                    e1.printStackTrace();
                }
            }
        } );
    }
}
