package skel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Login
{
    public Login()
    {
        
    }
    
    public boolean compruebaDatos ( String nombreFichero, String usuario, char[] contra ) throws IOException
    {
        File           ficheroDatos      = new File( nombreFichero );
        String         contraIntroducida = new String( contra );

        FileReader     ficheroDatosFR    = new FileReader( ficheroDatos );
        BufferedReader ficheroDatosBR    = new BufferedReader( ficheroDatosFR );

        String         lineaLeida;
        boolean        sigue             = ficheroDatosBR.ready();
        boolean        rv                = false;
        while( sigue )
        {
            lineaLeida = ficheroDatosBR.readLine(); // Le la primera linea. (usuario)
            if( lineaLeida.equals( usuario ) )      // Si la primera linea es el usuario comprueba la contrase�a.
            {
                if( comparaDatos( usuario, contraIntroducida, lineaLeida, ficheroDatosBR.readLine() ) ) // Si es su contrase�a para el bucle y devuelve true.
                {
                    sigue = false;
                    rv    = true;
                }else // Si no es su contrase�a para el bucle y devuelve false, ya que no puede haber nombres de usuarios repetidos.
                {
                    sigue = false;
                    rv    = false;
                }
            }else
                ficheroDatosBR.readLine();  // Si la lineaLeida no es el usuario, salta la siguiente, que ser�a la contrase�a.
            sigue = ficheroDatosBR.ready(); // Comprueba que haya mas lineas para seguir leyendo.
        }

        ficheroDatosBR.close();
        ficheroDatosFR.close();
        return rv;
    }

    private boolean comparaDatos ( String usuarioIntroducido, String contraConvertida, String usuarioFichero,
            String contraFichero )
    {
        boolean rv;
        if( usuarioIntroducido.equals( usuarioFichero ) && contraConvertida.equals( contraFichero ) )
            rv = true;
        else
            rv = false;
        return rv;
    }

}
