package skel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;



public class Fichero
{
    public Fichero ()
    {
    }

    
    /**
     * Lee todo el contenido del fichero linea por linea hasta llegar al final.</br>
     * 
     * Devuelve el contenido en un objeto String.
     * 
     * @param nombreFichero La ruta del fichero que se quiere leer.
     * @return String con el contenido del fichero leido.
     * @throws IOException 
     * 
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/io/File.html">Clase File</a>
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/io/FileReader.html">Clase FileReader</a>
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/io/BufferedReader.html">Clase BufferedReader</a>
     * 
     */
    public String leerFichero ( String nombreFichero ) throws IOException
    {

        String         contenido = "";
        String         buffer;
        File           fichero   = new File( nombreFichero );
        FileReader     ficheroFR;
        BufferedReader ficheroBR = null;

        if( fichero.exists() )
        {
            ficheroFR  = new FileReader( fichero );
            ficheroBR  = new BufferedReader( ficheroFR );
            buffer     = ficheroBR.readLine() + "\n";
            contenido += buffer;

            while( ( buffer = ficheroBR.readLine() ) != null )
                contenido += buffer + "\n";

            ficheroBR.close();
        }else
            contenido = "El fichero no existe";

        return contenido;
    }
    
    
    /** Escribe en un fichero un String que se le pasa por
     * argumento.
     * 
     * @param nombreFichero Un String con la ruta del fichero a escribir.
     * @param contenido El String de texto que queremos escribir en el fichero.
     * @throws IOException 
     *
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/io/File.html">Clase File</a>
     * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/io/FileWriter.html">Clase FileWriter</a>
     * @see <a https://docs.oracle.com/javase/7/docs/api/java/io/BufferedWriter.html">Clase BufferedWriter</a>
     */
    public void escribirFichero ( String nombreFichero, String contenido ) throws IOException
    {
        File           fichero = new File( nombreFichero );
        FileWriter     ficheroFW;
        BufferedWriter ficheroBW;

        if( fichero.exists() )
        {
            ficheroFW = new FileWriter( fichero );
            ficheroBW = new BufferedWriter( ficheroFW );
            ficheroBW.write( contenido );
            ficheroBW.close();
        }
    }

}
