﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurante
{
    class Restaurante
    {
        private const int TOTMESAS = 4;
        Mesa[] totalMesas;
        public Restaurante()
        {
            totalMesas = new Mesa[TOTMESAS];   // Reserva memoria.

            for (int i = 0; i < TOTMESAS; i++) // Instancia de los objetos.
                totalMesas[i] = new Mesa();
        }

        /** Recorre todas las mesas guardando los
         *  sitios ocupados y los libres para devolver
         *  la cantidad de sitios libres.
         */
        public int totalHuecosLibres()
        {
            int totalOcupadas = 0, totalMaximo = 0;

            for (int i = 0; i < TOTMESAS; i++)
            {
                totalOcupadas += totalMesas[i].getOcupantes();
                totalMaximo += totalMesas[i].getMaximos();
            }
            return totalMaximo - totalOcupadas;
        }

        public int totalOcupados()
        {
            int totalOcupadas = 0;
            for (int i = 0; i < TOTMESAS; i++)
                totalOcupadas += totalMesas[i].getOcupantes();
            return totalOcupadas;
        }

        public void ocupaMesa(int ocupantesNuevos, int numeroMesa)
        {
            if (totalMesas[numeroMesa].nuevoOcupante())
                for (int i = 1; i < ocupantesNuevos; i++)
                    this.totalMesas[numeroMesa].nuevoOcupante();
        }

    }
}
