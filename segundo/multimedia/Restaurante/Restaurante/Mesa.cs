﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurante
{
    class Mesa
    {
        private int ocupantesMaximos;
        private int ocupantesActuales;
        private bool mesaDisponible;
        private double cantidadCobro;

        public Mesa()
        {
            this.ocupantesMaximos = 4;      // Mesas de 4 personas.
            this.ocupantesActuales = 0;      // Mesa vacia.
            this.mesaDisponible = true;   // True -> Quedan huecos.
            this.cantidadCobro = 0;
        }

        public bool nuevoOcupante()
        {
            // Añadimos un nuevo ocupante a la mesa si quedan huecos.
            compruebaEstado();
            if (this.mesaDisponible)
                this.ocupantesActuales++;
            return this.mesaDisponible;
        }

        public void quitaOcupante()
        {
            this.ocupantesActuales--;
        }

        public int getOcupantes()
        {
            return this.ocupantesActuales;
        }
        public int getMaximos()
        {
            return this.ocupantesMaximos;
        }

        public void compruebaEstado()
        {
            if (this.ocupantesActuales < this.ocupantesMaximos)
                this.mesaDisponible = true;
            else
                this.mesaDisponible = false;
        }



    }
}
