﻿namespace Restaurante
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDisponible = new System.Windows.Forms.Button();
            this.btnOcupado = new System.Windows.Forms.Button();
            this.labelOcupado = new System.Windows.Forms.Label();
            this.labelDisponible = new System.Windows.Forms.Label();
            this.btnOcuparMesa = new System.Windows.Forms.Button();
            this.txbxNumMesa = new System.Windows.Forms.TextBox();
            this.labelNumMesa = new System.Windows.Forms.Label();
            this.labelNumOcupantes = new System.Windows.Forms.Label();
            this.txbxNumOcupantes = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txbxCantidadCobrar = new System.Windows.Forms.TextBox();
            this.txbxMesaCobrar = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCobrar = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txbxLog = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDisponible
            // 
            this.btnDisponible.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnDisponible.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDisponible.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnDisponible.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDisponible.Location = new System.Drawing.Point(23, 19);
            this.btnDisponible.Name = "btnDisponible";
            this.btnDisponible.Size = new System.Drawing.Size(210, 36);
            this.btnDisponible.TabIndex = 0;
            this.btnDisponible.Text = "Sillas Disponibles";
            this.btnDisponible.UseVisualStyleBackColor = false;
            this.btnDisponible.Click += new System.EventHandler(this.btnDisponible_Click);
            // 
            // btnOcupado
            // 
            this.btnOcupado.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnOcupado.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOcupado.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnOcupado.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOcupado.Location = new System.Drawing.Point(20, 20);
            this.btnOcupado.Name = "btnOcupado";
            this.btnOcupado.Size = new System.Drawing.Size(210, 36);
            this.btnOcupado.TabIndex = 0;
            this.btnOcupado.Text = "Sillas Ocupadas";
            this.btnOcupado.UseVisualStyleBackColor = false;
            this.btnOcupado.Click += new System.EventHandler(this.btnOcupado_Click);
            // 
            // labelOcupado
            // 
            this.labelOcupado.AutoSize = true;
            this.labelOcupado.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelOcupado.Location = new System.Drawing.Point(20, 59);
            this.labelOcupado.Name = "labelOcupado";
            this.labelOcupado.Size = new System.Drawing.Size(98, 25);
            this.labelOcupado.TabIndex = 1;
            this.labelOcupado.Text = "Ocupado:";
            // 
            // labelDisponible
            // 
            this.labelDisponible.AutoSize = true;
            this.labelDisponible.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelDisponible.Location = new System.Drawing.Point(23, 58);
            this.labelDisponible.Name = "labelDisponible";
            this.labelDisponible.Size = new System.Drawing.Size(112, 25);
            this.labelDisponible.TabIndex = 1;
            this.labelDisponible.Text = "Disponible:";
            // 
            // btnOcuparMesa
            // 
            this.btnOcuparMesa.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnOcuparMesa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOcuparMesa.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnOcuparMesa.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnOcuparMesa.Location = new System.Drawing.Point(21, 11);
            this.btnOcuparMesa.Name = "btnOcuparMesa";
            this.btnOcuparMesa.Size = new System.Drawing.Size(210, 36);
            this.btnOcuparMesa.TabIndex = 0;
            this.btnOcuparMesa.Text = "Ocupar Mesa";
            this.btnOcuparMesa.UseVisualStyleBackColor = false;
            this.btnOcuparMesa.Click += new System.EventHandler(this.btnOcuparMesa_Click);
            // 
            // txbxNumMesa
            // 
            this.txbxNumMesa.Location = new System.Drawing.Point(21, 83);
            this.txbxNumMesa.Name = "txbxNumMesa";
            this.txbxNumMesa.Size = new System.Drawing.Size(74, 23);
            this.txbxNumMesa.TabIndex = 2;
            // 
            // labelNumMesa
            // 
            this.labelNumMesa.AutoSize = true;
            this.labelNumMesa.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelNumMesa.Location = new System.Drawing.Point(21, 59);
            this.labelNumMesa.Name = "labelNumMesa";
            this.labelNumMesa.Size = new System.Drawing.Size(74, 21);
            this.labelNumMesa.TabIndex = 1;
            this.labelNumMesa.Text = "Nº Mesa";
            // 
            // labelNumOcupantes
            // 
            this.labelNumOcupantes.AutoSize = true;
            this.labelNumOcupantes.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labelNumOcupantes.Location = new System.Drawing.Point(116, 59);
            this.labelNumOcupantes.Name = "labelNumOcupantes";
            this.labelNumOcupantes.Size = new System.Drawing.Size(115, 21);
            this.labelNumOcupantes.TabIndex = 1;
            this.labelNumOcupantes.Text = "Nº Ocupantes";
            // 
            // txbxNumOcupantes
            // 
            this.txbxNumOcupantes.Location = new System.Drawing.Point(116, 83);
            this.txbxNumOcupantes.Name = "txbxNumOcupantes";
            this.txbxNumOcupantes.Size = new System.Drawing.Size(115, 23);
            this.txbxNumOcupantes.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.Controls.Add(this.txbxNumOcupantes);
            this.panel1.Controls.Add(this.txbxNumMesa);
            this.panel1.Controls.Add(this.labelNumOcupantes);
            this.panel1.Controls.Add(this.labelNumMesa);
            this.panel1.Controls.Add(this.btnOcuparMesa);
            this.panel1.Location = new System.Drawing.Point(13, 230);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 119);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel2.Controls.Add(this.labelOcupado);
            this.panel2.Controls.Add(this.btnOcupado);
            this.panel2.Location = new System.Drawing.Point(12, 118);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(249, 89);
            this.panel2.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel3.Controls.Add(this.labelDisponible);
            this.panel3.Controls.Add(this.btnDisponible);
            this.panel3.Location = new System.Drawing.Point(13, 15);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(248, 87);
            this.panel3.TabIndex = 6;
            // 
            // txbxCantidadCobrar
            // 
            this.txbxCantidadCobrar.Location = new System.Drawing.Point(116, 83);
            this.txbxCantidadCobrar.Name = "txbxCantidadCobrar";
            this.txbxCantidadCobrar.Size = new System.Drawing.Size(115, 23);
            this.txbxCantidadCobrar.TabIndex = 3;
            // 
            // txbxMesaCobrar
            // 
            this.txbxMesaCobrar.Location = new System.Drawing.Point(21, 83);
            this.txbxMesaCobrar.Name = "txbxMesaCobrar";
            this.txbxMesaCobrar.Size = new System.Drawing.Size(74, 23);
            this.txbxMesaCobrar.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(116, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cantidad €";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(21, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nº Mesa";
            // 
            // btnCobrar
            // 
            this.btnCobrar.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnCobrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCobrar.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnCobrar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCobrar.Location = new System.Drawing.Point(21, 11);
            this.btnCobrar.Name = "btnCobrar";
            this.btnCobrar.Size = new System.Drawing.Size(210, 36);
            this.btnCobrar.TabIndex = 0;
            this.btnCobrar.Text = "Cobrar Mesa";
            this.btnCobrar.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel4.Controls.Add(this.txbxCantidadCobrar);
            this.panel4.Controls.Add(this.txbxMesaCobrar);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.btnCobrar);
            this.panel4.Location = new System.Drawing.Point(13, 374);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(250, 119);
            this.panel4.TabIndex = 4;
            // 
            // txbxLog
            // 
            this.txbxLog.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txbxLog.Location = new System.Drawing.Point(296, 15);
            this.txbxLog.Multiline = true;
            this.txbxLog.Name = "txbxLog";
            this.txbxLog.ReadOnly = true;
            this.txbxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txbxLog.Size = new System.Drawing.Size(337, 478);
            this.txbxLog.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 514);
            this.Controls.Add(this.txbxLog);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(674, 553);
            this.MinimumSize = new System.Drawing.Size(674, 553);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Restaurante";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDisponible;
        private System.Windows.Forms.Button btnOcupado;
        private System.Windows.Forms.Label labelOcupado;
        private System.Windows.Forms.Label labelDisponible;
        private System.Windows.Forms.Button btnOcuparMesa;
        private System.Windows.Forms.TextBox txbxNumMesa;
        private System.Windows.Forms.Label labelNumMesa;
        private System.Windows.Forms.Label labelNumOcupantes;
        private System.Windows.Forms.TextBox txbxNumOcupantes;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txbxCantidadCobrar;
        private System.Windows.Forms.TextBox txbxMesaCobrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCobrar;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txbxLog;
    }
}

