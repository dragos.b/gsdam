﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Restaurante
{
    public partial class Form1 : Form
    {
        Restaurante rest = new Restaurante();
        int nLineaLog = 0;
        public Form1()
        {
            InitializeComponent();

        }

        private void btnDisponible_Click(object sender, EventArgs e)
        {
            labelDisponible.Text = "Disponible: " + rest.totalHuecosLibres();
        }

        private void btnOcupado_Click(object sender, EventArgs e)
        {
            labelOcupado.Text = "Ocupado: " + rest.totalOcupados();
        }

        private void btnOcuparMesa_Click(object sender, EventArgs e)
        {
            Int32 ocupantes, mesa;
            // El metodo IsNullOrEmpty devuelve true si la cadena de texto está vacia o contiene un null.
            if (!string.IsNullOrEmpty(txbxNumOcupantes.Text) && !string.IsNullOrEmpty(txbxNumMesa.Text))
            {
                ocupantes = Int32.Parse(txbxNumOcupantes.Text);
                mesa = Int32.Parse(txbxNumMesa.Text);
                rest.ocupaMesa(ocupantes, --mesa);
                txbxLog.Text += ++nLineaLog + ".-   Mesa " + (mesa + 1) + " ocupada por " + ocupantes + " personas";
                txbxLog.Text += Environment.NewLine;
            }

        }
    }
}
