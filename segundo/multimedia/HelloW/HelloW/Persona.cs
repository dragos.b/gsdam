﻿using System;

namespace HelloW {
    class Persona {
        // Private Atributtes
        private int edad;
        private String nombre, apellido;

        // Constructor
        public Persona ( int edad, String nombre, String apellido ) {
            this.edad = edad;
            this.nombre = nombre;
            this.apellido = apellido;
        }

        // Getters
        public int getEdad () => edad;  // The '=>' is equal to: ' int getEdad () { return edad; } '
        public String getNombre () => nombre;
        public String getApellido () => apellido;

        // Setters
        public void setEdad ( int edad ) {
            this.edad = edad;
        }

        public void setNombre ( String nombre ) {
            this.nombre = nombre;
        }

        public void setApellido ( String apellido ) {
            this.apellido = apellido;
        }

        // To Stirng
        public String toString => $"Te llamas {nombre} {apellido} y tienes {edad} años, un saludo !\n\n";
    }

}
