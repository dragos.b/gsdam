﻿namespace Temporizador
{
    partial class Ventana
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ventana));
            this.textBoxHoras = new System.Windows.Forms.TextBox();
            this.textBoxMinutos = new System.Windows.Forms.TextBox();
            this.textBoxSegundos = new System.Windows.Forms.TextBox();
            this.labelHoras = new System.Windows.Forms.Label();
            this.labelMinutos = new System.Windows.Forms.Label();
            this.labelSegundos = new System.Windows.Forms.Label();
            this.botonProgramar = new System.Windows.Forms.Button();
            this.botonCancelar = new System.Windows.Forms.Button();
            this.labelTitulo = new System.Windows.Forms.Label();
            this.labelHoraApagado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxHoras
            // 
            this.textBoxHoras.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBoxHoras.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHoras.Location = new System.Drawing.Point(41, 151);
            this.textBoxHoras.MaxLength = 1;
            this.textBoxHoras.Name = "textBoxHoras";
            this.textBoxHoras.Size = new System.Drawing.Size(32, 33);
            this.textBoxHoras.TabIndex = 1;
            this.textBoxHoras.Text = "0";
            this.textBoxHoras.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxMinutos
            // 
            this.textBoxMinutos.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMinutos.Location = new System.Drawing.Point(125, 151);
            this.textBoxMinutos.MaxLength = 2;
            this.textBoxMinutos.Name = "textBoxMinutos";
            this.textBoxMinutos.Size = new System.Drawing.Size(32, 33);
            this.textBoxMinutos.TabIndex = 2;
            this.textBoxMinutos.Text = "0";
            this.textBoxMinutos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSegundos
            // 
            this.textBoxSegundos.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSegundos.Location = new System.Drawing.Point(209, 151);
            this.textBoxSegundos.MaxLength = 2;
            this.textBoxSegundos.Name = "textBoxSegundos";
            this.textBoxSegundos.Size = new System.Drawing.Size(32, 33);
            this.textBoxSegundos.TabIndex = 3;
            this.textBoxSegundos.Text = "0";
            this.textBoxSegundos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelHoras
            // 
            this.labelHoras.AutoSize = true;
            this.labelHoras.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHoras.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelHoras.Location = new System.Drawing.Point(43, 121);
            this.labelHoras.Name = "labelHoras";
            this.labelHoras.Size = new System.Drawing.Size(29, 28);
            this.labelHoras.TabIndex = 4;
            this.labelHoras.Text = "H";
            // 
            // labelMinutos
            // 
            this.labelMinutos.AutoSize = true;
            this.labelMinutos.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMinutos.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelMinutos.Location = new System.Drawing.Point(125, 121);
            this.labelMinutos.Name = "labelMinutos";
            this.labelMinutos.Size = new System.Drawing.Size(34, 28);
            this.labelMinutos.TabIndex = 9;
            this.labelMinutos.Text = "M";
            // 
            // labelSegundos
            // 
            this.labelSegundos.AutoSize = true;
            this.labelSegundos.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSegundos.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelSegundos.Location = new System.Drawing.Point(214, 122);
            this.labelSegundos.Name = "labelSegundos";
            this.labelSegundos.Size = new System.Drawing.Size(25, 28);
            this.labelSegundos.TabIndex = 10;
            this.labelSegundos.Text = "S";
            // 
            // botonProgramar
            // 
            this.botonProgramar.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.botonProgramar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.botonProgramar.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botonProgramar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.botonProgramar.Location = new System.Drawing.Point(154, 212);
            this.botonProgramar.Name = "botonProgramar";
            this.botonProgramar.Size = new System.Drawing.Size(87, 44);
            this.botonProgramar.TabIndex = 4;
            this.botonProgramar.Text = "OK";
            this.botonProgramar.UseVisualStyleBackColor = false;
            this.botonProgramar.Click += new System.EventHandler(this.botonProgramar_Click);
            // 
            // botonCancelar
            // 
            this.botonCancelar.BackColor = System.Drawing.Color.DarkRed;
            this.botonCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.botonCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.botonCancelar.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.botonCancelar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.botonCancelar.Location = new System.Drawing.Point(41, 212);
            this.botonCancelar.Name = "botonCancelar";
            this.botonCancelar.Size = new System.Drawing.Size(87, 44);
            this.botonCancelar.TabIndex = 11;
            this.botonCancelar.Text = "NO";
            this.botonCancelar.UseVisualStyleBackColor = false;
            this.botonCancelar.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelTitulo
            // 
            this.labelTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTitulo.Font = new System.Drawing.Font("Microsoft YaHei", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitulo.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelTitulo.Location = new System.Drawing.Point(0, 0);
            this.labelTitulo.Name = "labelTitulo";
            this.labelTitulo.Size = new System.Drawing.Size(280, 81);
            this.labelTitulo.TabIndex = 12;
            this.labelTitulo.Text = "PROGRAMAR APAGADO";
            this.labelTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelHoraApagado
            // 
            this.labelHoraApagado.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHoraApagado.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelHoraApagado.Location = new System.Drawing.Point(12, 82);
            this.labelHoraApagado.Name = "labelHoraApagado";
            this.labelHoraApagado.Size = new System.Drawing.Size(260, 28);
            this.labelHoraApagado.TabIndex = 13;
            this.labelHoraApagado.Text = "hh-mm";
            this.labelHoraApagado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ventana
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.ClientSize = new System.Drawing.Size(284, 277);
            this.Controls.Add(this.labelHoraApagado);
            this.Controls.Add(this.labelTitulo);
            this.Controls.Add(this.botonCancelar);
            this.Controls.Add(this.botonProgramar);
            this.Controls.Add(this.labelSegundos);
            this.Controls.Add(this.labelMinutos);
            this.Controls.Add(this.textBoxMinutos);
            this.Controls.Add(this.textBoxHoras);
            this.Controls.Add(this.labelHoras);
            this.Controls.Add(this.textBoxSegundos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(300, 316);
            this.Name = "Ventana";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Apagado Automatico";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelHoras;
        private System.Windows.Forms.Label labelMinutos;
        private System.Windows.Forms.Label labelSegundos;

        private System.Windows.Forms.TextBox textBoxHoras;
        private System.Windows.Forms.TextBox textBoxMinutos;
        private System.Windows.Forms.TextBox textBoxSegundos;
        private System.Windows.Forms.Button botonProgramar;
        private System.Windows.Forms.Button botonCancelar;
        private System.Windows.Forms.Label labelTitulo;
        private System.Windows.Forms.Label labelHoraApagado;
    }
}

