﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Temporizador
{
    class Reloj
    {
        private byte horas, minutos, segundos;
        private int segundosTotales;

        public Reloj (byte horas, byte minutos, byte segundos = 10)
        {
            this.horas           = horas;
            this.minutos         = minutos;
            this.segundos        = segundos;
            this.segundosTotales = 0;
        }


        public void configuraTiempo(byte hora, byte minuto, byte segundo = 10)
        {
            this.horas    = hora;
            this.minutos  = minuto;
            this.segundos = segundo;
        }
        public void pasaSegundos()
        {
            this.segundosTotales = ( (this.horas * 60 * 60) + (this.minutos * 60) + this.segundos );
        }

        public void configuraApagado()
        {
            String comando = "/C shutdown /s /t " + this.segundosTotales;
            if (this.segundosTotales > 0)
            {
                 System.Diagnostics.Process.Start("cmd.exe", comando);
            }
        }

        public void cancelaApagado()
        {
            String comando = "/C shutdown /a";
            System.Diagnostics.Process.Start("cmd.exe", comando);
        }

        public void horaFutura( out int horaF, out int minutoF ) {
            DateTime horaActual = DateTime.Now;

            horaF   = (int) this.horas   + horaActual.Hour;
            minutoF = (int) this.minutos + horaActual.Minute;

            if (horaF >= 24)
            {
                horaF -= 24;
            }
            if ( minutoF >= 60 )
            {
                minutoF -= 60;
                horaF++;
            }
            
        }
    }
}
