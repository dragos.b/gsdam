﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Metodos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public int metodoA (double num)
        {
            num = Math.Truncate( num );
            return ( int ) num;
        }

        public void metodoB ( double num, out int rv)
        {
            num = Math.Truncate( num );
            rv = (int) num;
        }

        public void metodoC ( ref double num )
        {
            num = Math.Truncate( num );
        }

        public double metodoD ( double num, int tam )
        {
            return Math.Round( num, tam );
        }

        public void metodoE(double num, int tam, out double rv)
        {
            rv = Math.Round(num, tam);
        }
        public void metodoF(ref double num, int tam )
        {
            num = Math.Round( num, tam );
        }


        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            panelA.Visible = true;
            panelB.Visible = false;
            panelC.Visible = false;
            panelD.Visible = false;
            panelE.Visible = false;
            panelF.Visible = false;

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            panelA.Visible = false;
            panelB.Visible = true;
            panelC.Visible = false;
            panelD.Visible = false;
            panelE.Visible = false;
            panelF.Visible = false;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            panelA.Visible = false;
            panelB.Visible = false;
            panelC.Visible = true;
            panelD.Visible = false;
            panelE.Visible = false;
            panelF.Visible = false;
        }
        
        //TODO ->>
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            panelA.Visible = false;
            panelB.Visible = false;
            panelC.Visible = false;
            panelD.Visible = true;
            panelE.Visible = false;
            panelF.Visible = false;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            panelA.Visible = false;
            panelB.Visible = false;
            panelC.Visible = false;
            panelD.Visible = false;
            panelE.Visible = true;
            panelF.Visible = false;
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            panelA.Visible = false;
            panelB.Visible = false;
            panelC.Visible = false;
            panelD.Visible = false;
            panelE.Visible = false;
            panelF.Visible = true;
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            panelA.Visible = false;
            panelB.Visible = false;
            panelC.Visible = false;
            panelD.Visible = false;
            panelE.Visible = false;
            panelF.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            double num = double.Parse( textBox1.Text );
            textBox2.Text = metodoA(num).ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double num = double.Parse( textBox3.Text );
            int rv     = 0;
            metodoB(num, out rv);
            textBox4.Text = rv.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            double num = double.Parse( textBox5.Text );
            metodoC( ref num );
            textBox5.Text = num.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            double num, rv;
            int    tam;

            num = double.Parse ( textBox6.Text );
            tam = int.Parse( textBox7.Text );
            rv =metodoD(num, tam);
            textBox8.Text = rv.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            double num, rv_ref;
            int tam;
            num = double.Parse(textBox9.Text);
            tam = int.Parse(textBox10.Text);
            metodoE( num, tam, out rv_ref );
            textBox11.Text = rv_ref.ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            double num;
            int tam;
            num = double.Parse (textBox12.Text);
            tam = int.Parse(textBox13.Text);
            metodoF(ref num, tam);
            textBox12.Text = num.ToString();
        }
    }
}
