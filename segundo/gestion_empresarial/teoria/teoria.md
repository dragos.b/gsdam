# Sistemas de Gestión Empresarial

## Definición

Los ERP o **Enterprise Resource Planning** son un conjunto de herramientas que
se usan para llevar a cabo todos los procesos dentro de una empresa
(contabilidad, gestión empleados, comunicación con clientes y proveedores,
adminsitración, etc).

## Modulos

Estos sistemas se organizan mediante módulos, estos mismos se dividen por
procesos que llevan a cabo las empresas.

Los módulos se comunican entre sí.

### MIS Management Information Systems

Este módulo es muy util cuando una empresa cuenta con tarjetas de
visita, cualquier tipo de publicidad, una web, firmas en e-mail,
cabeceras en los documentos y cosas generales de la empresa.

Cuenta con la utilidad si en algún momento se **modifica** el nombre,
el logo o cualquier cosa general de la empresa se **cambia en todos
los sitios** configurados.

### CRM Customer Relationship Management

Este módulo trata de la relación con los clientes, todos sus datos,
comunicaciones que se hacen con ellos, acuerdos, etc.

Un ejemplo muy útil es si un empleado de turno de mañana llega a un
acuerdo con un cliente y no lo documenta en ningún sitio, el cliente
vuelve a ponerse en contacto con la empresa por la tarde y el empleado
de tarde no conoce nada del acuerdo al que llegaron por la mañana.

### PLM Product Lifecycle Management

Es la gestión del ciclo de vida de un producto, desde que lo creamos
hasta que se deja de fabricar.

Se divide en varias fases:

1. Diseño **conceptual**
1. Diseño **real y simulaciones**
1. **Fabricación y pruebas de calidad/seguridad**
1. **Servicio** *(Distribucion, Venta, Soporte, Mantenimiento, Retirada)*


### SCM Supply Chain Management

Módulo que se encarga del suministro, en este módulo por ejemplo
se hacen busquedas de un material en concreto y podemos encontrar
el proveedor más barato.

Tambén evita interrupciones en la cadena de suministro, llevando a
cabo la **logística, inventario y aprovisionamieto.**


### SRM Supplier Relationship Management

Este módulo se encarga de la **relación con proveedores**, es parecido
a la de clientes, gestiona pedidos, proveedores, facturas, negociaciones etc.


### MRP Material Requirements Planning

Son los requisitos que nos hacen falta para fabricar un producto,
es decir los materiales.

* 100 x tornillos
* 50  x tablas
* 2   x pomos

### MRP II Manufacture Resource Planning

Este módulo es mas avanzado ya que permite la optimización a la
hora de fabricar.

Previene errores tanto para maquinaria como errores humanos, también
permite la mejor gestion de porciones o cantidades de material.

### POS Point Of Sale

Son los Puntos de Venta que por lo general existen en tiendas,
restaurantes, etc.

### CMS Content Management System

Son gestores de contenido que crean un entorno de trabajo
para administrar y crear contenidos, por lo general webs,
foros, blogs y comercios.

Algunos ejemplos son `WordPress, Joomla, Drupal, Moodle, MediaWiki, NextCloud`

### Blog weB log

Es un diario web en el que se publican entradas y se muestran por fecha
descendiente.

### eCommerce

Comercio electrónico

### PRM Partnership Realtionshop Management

Gestiona la relación con socios u otras empresas, que por ciertos motivos
comienzan a colaborar o a hacer un proyecto juntos.

### KM Knowledge Management

Este módulo guarda y gestiona como se hacen ciertas cosas en una empresa.

Por ejemplo una receta de mezcla de colores, o de alimentos, como se fabrica
cierta pieza.

Guarda manuales que se deben seguir para llegar a una solución.

### BI Business Inteligence

Gestiona la inteligencia empresarial, por ejemplo cuando bajar el precio
de los productos, la cantidad del descuento.


## Comparativa ERPs Libres & Propietarios


| ERP  | SGBD  | Web  | Comunicación Cliente/Servidor  | Lenguaje  | Arquitectura  | Licencia | Cliente  | Personalización |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| Odoo version 13.0 | PostgreSQL  | Si  | XML & JSON | Python & XML | Web | GNU LGPL  | Buenas valoraciones menos en algunos modulos que son de pago | Configuración y Creación de módulos en Python |
| Openbravo  | MySQL, PostgreSQL o Oracle  | Si  | XML | Java  | Web y Movil / MVC y Orientado a Modelos | Openbravo Public License (MPL)  | Gran crítica *se arpovecha del codigo de la comunidad y no aportan nada a ella*  |   |
| WebERP  | MySQL  | Si  | HTTP/S  | PHP  | Web Cliente-Servidor  | GNU GPL  | 34 Idiomas, Interfaz Débil  | Modificación y creación de modulos en PHP  |
| Tryton  | PostgreSQL  | No  | PYSON  | Python | 3 Capas (Cliente, Servidor y BBDD), SaaS, Nube, on-premise  | GNU GPL  |   | Configuración y Creación de módulos en Python y XML  |
| Dolibarr  | MySQL o PostgreSQL  | Si  | HTTP  | PHP/HTML  | REST API | GNU GPL  |   | Se permite el desarollo de nuevos módulos  |
| Oracle ERP  | Oracle  | No  |   | C  PL/SQL | on-premise y en nube  | Privativas  | Servicio técnico 24/7  | Configuración y a medida mediante pagos  |
| SAP  | SAP HANA, Oracle, IBM DB/2, M. SQL Server  | No  | TCP/IP, RPC, SQL  | C, C++, ABAP  | Aplicación de OS SAP GUI  | Privativas |   | Configuración de lo que la empresa permite  |
| Microsoft Dynamics  | Microsoft SQL Server  | Si  | XML, JSON  | .NET | Multidispositivo | Privativas  | Gran integración con Office 365  | Configuración  |



## Comparativa CRMs

| CRM | SGBD  | Web  | Comunicación Cliente/Servidor  | Lenguaje  | Arquitectura  | Licencia | Cliente  | Personalización | Independiente/Integrado |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| Odoo CRM  | PostgreSQL  | Si  | XML & JSON  | Python & XML  | Web  | GNU LGPL  |   | Se puede modificar con Python | Integrado |
| SalesForce  | No Info, se pueden hacer consultas SQL y SOQL | Si  |   | No Info, para personalizar se utiliza Apex / Visualforce  | Basado en Nube| Privativas  |   | Permite personalización grafica con Visualforce y añadir funcionalidades con Apex | Independiente  |
| ZOHO CRM  | No info pero se pueden volcar datos desde BBDD SQL y NoSQL | Si  |   |   | SaaS  | Privativas  |   | Se pueden programar "automatizaciones" dentro del cliente  | Independiente |
| SugarCRM  | MySQL, MS SQL Server, Oracle  | Si  | PHP  | PHP  | SaaS  | PL SugarCRM y Privativas  |   |   | Independiente |
| Pipedrive  | MySQL  | Si  | JSON  | JavaScript, PHP  | SaaS  | Privativas  |   |   | Independiente |


## SGBD Compatibles con Odoo

Por defecto Odoo solo es compatible con PostgreSQL, en versiones anteriores (OpenERP) se podía utilizar con MySQL.

Existen Apps de Odoo que permite utilizar MySQL, uno es `External Database Source - MYSQL`, se crean conexiones
externas usando `ODBC, Firebird, Oracle Client o SQL Alchemy`, la app de MySQL hace uso de `SQL Alchemy`.

Se pueden utilizar los siguientes sistemas:

1. MySQL
1. Oracle
1. SQLite
1. Microsoft SQL Server

## SGBD Compatibles con ERP

1. PostgreSQL
1. Oracle
1. Microsoft SQL Server
1. MySQL
1. MariaDB
1. SQLite
1. SAP HANNA
1. IBM DB

## Google Trends


En el ultimo año podemos observar los ERPs que más tendencia han tenido en la busqueda de Google a nivel mundial.


![ERPs Tendencias](erp_trends.png)

**Leyenda**
* **AZUL:** `Odoo`
* **ROJO:** `SAP ERP`
* **AMARILLO:** `Microsoft Dynamics`
* **VERDE:** `Oracle ERP`


En el siguiente gráfico observamos mas en detalle los paises que mas tendencia han tenido buscando `Odoo`

![Odoo Tendencias Mundiales](odoo_trends_country.png)


En España vemos donde mas inteŕes existe por `Odoo`

![Odoo Tendencias España](odoo_spain.png)


Para ver más detalles dejo el [enlace a Google Trends](https://trends.google.com/trends/explore?geo=ES&q=%2Fg%2F11bc5dd_bx,SAP%20ERP,%2Fm%2F07wxy9,Oracle%20ERP)



## Nomenclatura ODOO


En odoo cuando creamos una Solicitud de Presupuesto, un Pedido, una Venta, una entrada al almacén o salida
se generan un codigo de referencia que consta de un codigo corto para saber a que se refiere.


#### SO Sale Order

Orden de Venta, se genera cuando vendemos productos

#### PO Purchase Order

Orden de compra, se genera cuando creamos una solicitud de presupuesto


#### WH WareHouse

Es nuestro almacén

#### IN/OUT

Siempre va acompañado del almacen, WH/IN o WH/OUT, y hacen referencia al albarán
de recibido/entregado.

IN quiere decir que lo hemos recibido.

OUT quiere decir que sale del almacén.


## Ventas/Compras

Al comprar y vender productos pueden ser de 3 tipos **Consumibles, Almacenables y Servicios**

#### Consumibles

1. **Poca cantidad**
1. **Se compran cuando se terminan**
1. **No se lleva conteo de stock**
1. **No son la base del negocio**

*En una gasolinera como consumibles pueden ser los productos del stand de venta, (chicles, pendrives, boligrafos...)*

#### Almacenables

1. **Gran volumen de compra/venta**
1. **Productos principales del negocio**
1. **Gran uso**
1. **Precio más bajo**
1. **Se lleva un conteo de stock**
1. **Se pueden automatizar las compras**

*En un bar por ejemplo los refrescos o la comida, existe un volumen alto de compra/venta*

### Reabastecimiento

Cuando vamos a comprar uno o varios productos hay dos maneras de compra, de **Stock** y **Bajo Pedido**

#### Stock

Significa que ese producto es limitado y tendremos que saber si existen unidades a la venta y a continuación
realizar el pedido.

*Las fresas de temporada por ejemplo son un producto de stock, ya que debemos saber si al agricultor le quedan para poder comprar*

#### Bajo Pedido

Son productos ilimitados que podremos adquirirlo sin comprobar que siguen existiendo unidades.

*Los refrescos serían un producto bajo pedido, ya que haríamos un pedido a la fabrica y esta nos lo enviaría*

### CRM

#### Iniciativa

Es un contacto que podría convertirse en una oportunidad de negocio.

Ejemplo:

* Correos de Publicidad
* Anuncios Publicitarios
* Carteles

#### Oportunidad

Es una posibilidad de **negocio concreta**, que consta de **estimación de ingresos** y una
**probabilidad** de llegar a ser un Pedido de Venta.

Las oportunidades se dividen por etapas:

* **Nuevo**: Se crea la oportunidad.
* **Calificación**: Configuración de parámetros, *estimacion de ingresos, tiempo...*
* **Propuesta**: Se hace una oferta al cliente.
* **Negociación**: Se negocia la oferta con el cliente.
* **Ganado**: Se convierte en **Pedido de Venta**.
* **Perdido**: Oportunidad Perdida.
