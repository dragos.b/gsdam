package agencia;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Scanner;
import com.google.common.collect.HashMultimap;


enum TOrden
{
    MENOR , MAYOR 
};

public class OpcionesMenu
{
    private HashMultimap <String, String> itinerariosDestinos;
    private Scanner                 sc = new Scanner( System.in );
    private FicheroItinerario fi = new FicheroItinerario();
   
    public OpcionesMenu ()
    {
        itinerariosDestinos = HashMultimap.create();
    }

    /** Abre un fichero que le indiquemos en modo lectura.
     * Si el fichero no existe lo crea.
     * 
     * Si existe lo lee por bloques y guarda su contenido en una 
     * estructura HashMap<String><String>.
     * 
     * Cuando llega al final del fichero deja de leer y lo cierra.
     */
    public void leeGuarda ()
    {
        System.out.println( "Indica nombre fichero" );
        sc.reset();
        String nombreFichero = sc.nextLine();
        try
        {
            fi.abrir( nombreFichero, TApertura.LECTURA ); // Abre en modo Lectura
            while( !fi.finalF() )                                                 // Si llega al final termina
                fi.leer( itinerariosDestinos );                            // Lee bloque1, bloque2, bloque3, bloque4....
            fi.cerrar();                                                              // Cierra el fichero.
        }catch( IOException e )
        {
            System.out.println( "Ha habido un problema, prueba de nuevo." );
        }
    }

    public void guardaFichero ( String nombreFichero ) throws IOException
    {
        ArrayList<String> itinerarios = new ArrayList<String>();
        // Abre el fichero en modo Escritura.
        fi.abrir( nombreFichero, TApertura.ESCRITURA );
        
        // Guarda los Itinerarios, ya que en el HashMap están repetidos.
        for ( String i: itinerariosDestinos.values() )
            if( !itinerarios.contains( i ) )
                itinerarios.add( i );
        // Recorre los Itinerarios y escribe cada uno en el fichero.
        for ( int i = 0; i < itinerarios.size(); i++ )
            fi.escribir( itinerarios.get( i ), itinerariosDestinos );
        fi.cerrar();
    }

    public void insertaItinerario ()
    {
        ArrayList<String> destinos = new ArrayList<String>();

        System.out.println( "Nombre Itinerario:" );
        String nombre = sc.nextLine();
        nombre  = nombre.toUpperCase();  //  Todos los Itinerarios a mayusuclas.

        System.out.println( "Destinos:\n\tPara Terminar Introducir: 'X' (sin comillas)" );
        
        String buff = sc.nextLine();
        
        while ( !buff.contains( "X" ) ) // Guarda datos introducidos por teclado hasta que encuentra una 'X'.
        {
            destinos.add( buff.toLowerCase() );  // Guarda los Destinos del Itinerario en minusculas.
            buff = sc.nextLine();                           //  Vuelve a leer
        }

        // Recorre los Destinos y los añade a la estrucutra HashMap, todos en el mismo itinerario.
        for ( int i = 0; i < destinos.size(); i++ )
            itinerariosDestinos.put( destinos.get( i ), nombre );
    }

    /**
     * Pide al usuario un Itinerario para eliminar y recorre la estrucutra HashMap buscando las Keys que
     * su Value sea el Itinerario introducido por el usuario, si es así lo elimina.
     */
    public void eliminaItinerario ()
    {
        String  destino, itinerario;
        Iterator<String> it   = itinerariosDestinos.keySet().iterator();  // Lo uso para iterar en el bucle.
        
        System.out.println( "Introduce itinerario a eliminar:" );
        itinerario = sc.nextLine();
        itinerario = itinerario.toUpperCase();
        
        while( it.hasNext() ) // Mientras haya un proximo en 'iterador'.
        {
            destino = it.next();
            if( itinerariosDestinos.containsEntry( destino, itinerario ) ) // Comprueba si existe el dato <K><V> y lo borra.
                it.remove();
        }
    }

    public void vaciaRam ()
    {
        itinerariosDestinos.clear();
    }

    public void separarFichero () throws IOException
    {
        int separacionDestinos, totalDestinos;
        ArrayList<String> itinerarios = new ArrayList<String>();
        itinerarios = totalItinerarios();
        String menor = "", mayor = "";
        
        System.out.println( "Indica por cuantos destinos separar los itinerarios" );
        separacionDestinos = sc.nextInt();
        
        // Instanciación de objetos para escribir.
        File ficheroMenor   = new File( "menor_" + separacionDestinos + ".txt" );
        File ficheroMayor   = new File( "mayor_" + separacionDestinos + ".txt" );
        FileWriter ficheroMenorW  = new FileWriter( ficheroMenor );
        FileWriter ficheroMayorW  = new FileWriter( ficheroMayor );
        BufferedWriter ficheroMenorBW = new BufferedWriter( ficheroMenorW );
        BufferedWriter ficheroMayorBW = new BufferedWriter( ficheroMayorW );

        ficheroMenor.createNewFile();
        ficheroMayor.createNewFile();

        // Recorre el itinerario y se guarda los itinerarios que sean menores o
        // iguales que la separación en el String 'menor' y los demás en el String
        // 'mayor'
        for ( int i = 0; i < itinerarios.size(); i++ )
        {
            totalDestinos = cuentaDestinos( itinerarios.get( i ) );
            if( totalDestinos < separacionDestinos )
            {
                menor += itinerarios.get( i ) + "\n";
                menor += Integer.toString( totalDestinos ) + "\n";
                menor += leeItinerario( itinerarios.get( i ) );
            }else
            {
                mayor += itinerarios.get( i ) + "\n";
                mayor += Integer.toString( totalDestinos ) + "\n";
                mayor += leeItinerario( itinerarios.get( i ) );
            }
        }
        // Escribe los ficheros.
        ficheroMenorBW.write( menor );   // menor_N.txt
        ficheroMayorBW.write( mayor );   // mayor_N.txt

        // Cierra todos los Streams de escritura.
        ficheroMenorBW.close();
        ficheroMayorBW.close();
        ficheroMenorW.close();
        ficheroMayorW.close();
        
    }

    public void modificaItinerario( String destino, String itinerarioViejo, String itinerarioNuevo )
    {
        // Elimina el Destino del Itinerario indicado y lo vuelve a añadir con el nuevo Itinerario.
        itinerariosDestinos.remove( destino, itinerarioViejo.toUpperCase() );
        itinerariosDestinos.put( destino, itinerarioNuevo.toUpperCase( ) );
    }
    
    public String leerDatosRam ()
    {
        String            retorno     = "";
        ArrayList<String> itinerarios = new ArrayList<String>();
        itinerarios = totalItinerarios();  // Itinerarios sin repetir.

        // Recorre los Itinerarios y guarda en un String todos los datos
        // Cuando ha recorrido todo devuelve el String.
        for ( int i = 0; i < itinerarios.size(); i++ )
        {
            retorno += itinerarios.get( i ) + "\n";
            retorno += leeItinerario( itinerarios.get( i ) ) + "\n";
        }
        return retorno;
    }

    public void mostrarPorOrden ( TOrden tipoOrden )
    {
        HashMap<String,Integer> itinerarioDestinos = new HashMap<String,Integer>();
        ArrayList<String>              itinerariosTotales  = new ArrayList<String>();
        String                  tituloItinerario   = "";
        String                  imprime            = "";
        itinerariosTotales = totalItinerarios();
        int numeroTotal = 1;

        // Dependiendo del orden seleccionado configura la numeroTotal
        // En busca de Menores o Mayores.
        if( tipoOrden == TOrden.MENOR )
            numeroTotal = 1000000;
        else if( tipoOrden == TOrden.MAYOR )
            numeroTotal = 0;

        // Añade al HashMap 'itinerarioDestinos' los datos correspondientes a  cada itinerario.
        // Como Clave el Itinerario y como Valor la cantidad de Destinos.
        for ( int i = 0; i < itinerariosTotales.size(); i++ )
            itinerarioDestinos.put( itinerariosTotales.get( i ), cuentaDestinos( itinerariosTotales.get( i ) ) );
            //   ^---HashMap               ^------Itinerario X                            ^---------- Cantidad Destinos
        
        for ( int i = 0; i < itinerariosTotales.size(); i++ )       // Recorre los Itinerarios (ESPAÑA, AMERICA, MEDITERRANEO)
        {
            for ( String iti: itinerarioDestinos.keySet() )         // Guarda el itinerario en 'iti' y comprueba la cantidad de destinos.
            {
                if( tipoOrden == TOrden.MENOR ) // Comprueba si queremos ordenar de menor a mayor.
                {
                    if( ( itinerarioDestinos.get( iti ) <= numeroTotal ) ) // Verifica si el numero de destinos es menor que el
                                                                                                        // más mayor.
                    {
                        numeroTotal    = itinerarioDestinos.get( iti );
                        tituloItinerario = iti;
                    }
                }else if( tipoOrden == TOrden.MAYOR ) // Comprueba si queremos ordenar de mayor a menor.
                {
                    if( ( itinerarioDestinos.get( iti ) > numeroTotal ) )
                    {
                        numeroTotal    = itinerarioDestinos.get( iti );
                        tituloItinerario = iti;
                    }
                }
            }
            
            // Introduce los datos obtenidios en el String 'imprime', elimina el Itinerario recién usado 
            // del HashMap itinerarioDestinos.
            // También resetea la variable 'numeroTotal' según le corresponda.
            imprime += tituloItinerario + "\n";
            imprime += Integer.toString( numeroTotal ) + "\n";
            imprime += leeItinerario( tituloItinerario ) + "\n";
            itinerarioDestinos.remove( tituloItinerario ); 
            // Elimino el ultimo Itinerario porque si no siempre saldría ese mismo.

            if( tipoOrden == TOrden.MENOR )
                numeroTotal = 1000000; // Valor muy alto para averiguar numero más bajo.
            else if( tipoOrden == TOrden.MAYOR )
                numeroTotal = 0; // Valor muy bajo para averiguar numero más alto.
        }
        System.out.println( imprime );
    }
    
    /** Imprime el Destino más repetido en memoria y la cantidad
     *  de repeticiones.
     */
    public void destinoRepetido()
    {
        HashMap<String,Integer> destinosTotales = new HashMap<String,Integer>();
       
        // Añado a destinosTotales todos los destinos con un 0 en la cantidad
        // de repetido (value).
        for ( String d: itinerariosDestinos.keySet() )
            destinosTotales.put( d, 0 );
  
        // Recorre todos los datos del itinerario.
        for ( Entry<String,String> d: itinerariosDestinos.entries() )
            // Comprueba si en destinosTotales existe el destino 'p'
            if( destinosTotales.containsKey( d.getKey() ) )
                // Si existe el destino, hace un put del mismo destino con su anterior
                // cantidad (value) + 1.
                destinosTotales.put( d.getKey(), destinosTotales.get(d.getKey()) + 1 );    

        int mayor = 0;
        String destinoMayor = null;
        for ( String d: destinosTotales.keySet() )
            if ( destinosTotales.get( d ) > mayor )
            {
                mayor = destinosTotales.get( d );
                destinoMayor = d;
            }
        
        // Si hay algún destino repetido lo imprime, si no, imprime otra cosa.
        if ( mayor > 1)
            System.out.println( "Destino más repetido:\n" + destinoMayor + " : " + mayor + " veces");
        else
            System.out.println( "Ningún destino repetido" );
    }
    
    // METODOS INTERNOS
    private  ArrayList<String> totalItinerarios ()
    {
        // Devuelve solamente los itinerarios  en un ArrayList (MEDITERRANEO, EUROPA, AMERICA...)
        ArrayList<String> itinerarios = new ArrayList<String>();

        for ( String v: itinerariosDestinos.values() )
            if( !itinerarios.contains( v ) )   // Si el ArrayList no contiene el Itinerario v, lo añade
                itinerarios.add( v );
        return itinerarios;
    }
    private int cuentaDestinos ( String titulo  )
    {
        /*
         * Se le pasa un itinerario: EUROPA, MEDITERRANEO... y devuelve el total de destinos que consta en
         * ese itinerario.
         */
        int cont = 0;
        for ( String v: itinerariosDestinos.values() )
        {
            if( v.equalsIgnoreCase( titulo ) )
                cont++;
        }
        return cont;
    }
    private String leeItinerario ( String itinerario )
    {
        /*
         * Devuelve todos los destinos de un itinerario en especifico
         */
        String retorno = "";

        for ( String i: itinerariosDestinos.keySet() )
            if( itinerariosDestinos.containsEntry( i, itinerario ) )
                retorno += i + "\n";
        return retorno;
    }
}