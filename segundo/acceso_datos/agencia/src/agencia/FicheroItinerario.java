package agencia;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import com.google.common.collect.*;



enum TApertura
{
    LECTURA, ESCRITURA
};

public class FicheroItinerario
{

    private TApertura      tipoApertura;
    private File           fichero;
    private FileReader     ficheroFR;
    private BufferedReader ficheroBR;
    private FileWriter     ficheroFW;
    private BufferedWriter ficheroBW;

    public FicheroItinerario ()
    {
    }

    /**
     * Abre el fichero que le indiquemos de Lectura o Escritura.
     * 
     * @param nombreFichero La ruta del fichero para abrir.
     * @param tipoAbierto   El tipo que apertura con la que vamos a abrir el fichero ( TApertura.LECTURA
     *                      / TApertura.ESCRITURA )
     * @throws IOException
     */
    public void abrir ( String nombreFichero, TApertura tipoAbierto ) throws IOException
    {
        this.fichero = new File( nombreFichero );

        if( !fichero.exists() ) // Si no existe el fichero lo crea.
            fichero.createNewFile();

        if( tipoAbierto == TApertura.LECTURA )
        { // Lo abre en modo lectura
            tipoApertura = TApertura.LECTURA;
            ficheroFR    = new FileReader( fichero );
            ficheroBR    = new BufferedReader( ficheroFR );

        }else if( tipoAbierto == TApertura.ESCRITURA )
        { // Lo abre en modo escritura
            tipoApertura = TApertura.ESCRITURA;
            ficheroFW    = new FileWriter( fichero );
            ficheroBW    = new BufferedWriter( ficheroFW );
        }
    }

    /**
     * Lee por bloques el fichero y lo guarda en el objeto itinerarioDestino.
     * 
     * @param itinerarioDestino
     * @throws IOException
     */
    public void leer ( HashMultimap <String, String> itinerarioDestino ) throws IOException
    {
        String itinerarioTitulo;
        int    totalDestinos;
        if( fichero.exists() )
        {
            itinerarioTitulo = ficheroBR.readLine();                               // Primera Linea ( itinerario_1, ej: Europa ).
            itinerarioTitulo = itinerarioTitulo.toUpperCase();                // Conversion del nombre de Itinerario a mayusculas.
            totalDestinos    = Integer.parseInt( ficheroBR.readLine() );  // Cantidad  de destinos convertida a numero entero.

            for ( int i = 0; i < totalDestinos; i++ ) // Recorre el fichero y guarda en nuestra estructura HashMultimap.
                itinerarioDestino.put( ficheroBR.readLine().toLowerCase(), itinerarioTitulo.toUpperCase() );
        }
    }

    /**
     * Escribe en el fichero un itinerario en concreto desde el itinerarioOrigen.
     * 
     * @param itinerarioEscribir Cadena de texto especifiicando que itinerario deseamos escribir.
     * @param itinerarioOrigen La estructura de datos que almacena todos los itinerarios.
     * @throws IOException
     */
    public void escribir ( String itinerarioEscribir, HashMultimap <String, String> itinerarioOrigen )
            throws IOException
    {
        int totalDestinos = cuentaDestinos( itinerarioEscribir, itinerarioOrigen );
        ficheroBW.write( itinerarioEscribir.toUpperCase() + "\n" );      // Escribe Itinerario en mayúsculas.
        ficheroBW.write( Integer.toString( totalDestinos ) + "\n" );      // Escribe el total de Destinos en forma de String ya que si no, escribe un carácter ASCII.
        ficheroBW.write( leeItinerario( itinerarioEscribir, itinerarioOrigen ) ); // Escribe todos los destinos.
        ficheroBW.flush();
    }

    /**
     * Comprueba como ha sido creado el fichero. </br>
     * 
     * De Lectura o Escritura, y cierra los objetos FileWriter/BufferedWriter o FileReader/BufferedReader
     * 
     * @throws IOException
     */
    public void cerrar () throws IOException
    {
        if( tipoApertura == TApertura.LECTURA )
        { // Cierra los streams de lectura y deja los objetos apuntando a null.
            ficheroFR.close();
            ficheroBR.close();
            ficheroFR = null;
            ficheroBR = null;
        }else if( tipoApertura == TApertura.ESCRITURA )
        { // Cierra los streams de escritura y deja los objetos apuntando a null.
            ficheroFW.close();
            ficheroBW.close();
            ficheroFW = null;
            ficheroBW = null;
        }
    }

    /** Comprueba si ha llegado al final del archivo con fichero
     * 
     * @return Verdadero si es el final del archivo, falso si no es el final.
     * @throws IOException
     */
    public boolean finalF () throws IOException
    {
        boolean rv;
        if( !ficheroBR.ready() )
            rv = true;
        else
            rv = false;
        return rv;
    }

    /** Simula que se vacia el fichero. </br>
     * Se elimina e inmediatamente se vuelve a crear.
     * @throws IOException
     */
    public void vaciar () throws IOException
    {
        if( fichero.delete() )
            fichero.createNewFile();
    }

    // METODOS INTERNOS
    
    /** Recorre un Itinerario en concreto y cuenta el total de destinos que tiene.
     * @param itinerario 
     * @param estructuraItinerario
     * @return Total de Destinos en un Itinerario
     */
    private int cuentaDestinos ( String itinerario, HashMultimap <String, String> estructuraItinerario )
    {
        int cont = 0;
        for ( String v: estructuraItinerario.values() )
        {
            if( v.equalsIgnoreCase( itinerario ) )
                cont++;
        }
        return cont;
    }

    /** Recorre un Itinerario y devuelve en un String todos los destinos del mismo, separados por saltos de linea.
     * 
     * @param itinerario
     * @param estructuraItinerario
     * @return String de los destinos del itinerario que se le indica en el parametro.
     */
    private String leeItinerario ( String itinerario, HashMultimap<String,String> estructuraItinerario )
    {
        String retorno = "";

        for ( String i: estructuraItinerario.keySet() )
            if( estructuraItinerario.containsEntry( i, itinerario ) )
                retorno += i + "\n";
        return retorno;
    }
}
