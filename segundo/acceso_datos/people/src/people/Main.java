package people;

public class Main
{

    public static void main ( String[] args )
    {
        ListPeople people = new ListPeople();

        people.createPerson( "Antonio", "Cantizano", 22, "spain" );
        people.createPerson( "Jaime", "Sanchez", 22, "spain" );
        people.createPerson( "Alejandro", "torres", 10, "pais" );

        people.createFile( "personas.txt" );
        people.writeFile();
        people.readFile();
    }

}
