package directorios;

import java.io.File;
import java.io.IOException;



public class Main
{

    public static void main ( String[] args )
    {
        String[] directorios = {
                "Proyecto",
                "Proyecto/estilos",
                "Proyecto/script", 
                "Proyecto/imagenes",
                
                "Proyecto/estilos/css4", 
                "Proyecto/estilos/css3", 
                
                "Proyecto/script/js", 
                "Proyecto/script/php",
                
                "Proyecto/imagenes/alta", 
                "Proyecto/imagenes/media", 
                "Proyecto/imagenes/baja" 
                };
        
        String[] ficheros = {
                "Proyecto/estilos/css4/estilo1.css",
                "Proyecto/estilos/css4/estilo2.css",
                "Proyecto/estilos/css3/estilo2.css",
                
                "Proyecto/script/js/script1.js",
                "Proyecto/script/js/script2.js",
                "Proyecto/script/php/cabecera.php",
                "Proyecto/script/php/modelo.php",
                
                "Proyecto/imagenes/alta/paisaje.jpg",
                "Proyecto/imagenes/media/paisaje.jpg",
                "Proyecto/imagenes/baja/paisaje.jpg",
                
        };
        
        String[][] directoriosFicheros = 
            { 
                { "Proyecto" }, 
                { "/estilos", "/css4", "/css3" },
                { "/script", "/js", "/php" },
                { "/imagenes", "/alta", "/media", "/baja" },
            };
        

        
        // Segunda Implementacion
        int        size;
        for ( int i = 0; i < directoriosFicheros.length; i++ )
        {
            size = directoriosFicheros[i].length;
            for ( int j = 1; j < size; j++ )
            {
                // TODO eliminar sysout, es solo para testear
                System.out.println( directoriosFicheros[0][0] + directoriosFicheros[i][0] + directoriosFicheros[i][j] );

                File tempDir = new File(
                        directoriosFicheros[0][0] + directoriosFicheros[i][0] + directoriosFicheros[i][j] );
                tempDir.mkdirs();
            }
        }      
        
        
        
        // Primera implementacion
        /*
        for ( int i = 0; i < directorios.length; i++ )
        {
            File temp = new File( directorios[i] );
            temp.mkdir();
        }

        for ( int i = 0; i < ficheros.length; i++ )
        {
            File temp = new File( ficheros[i] );
            try
            {
                temp.createNewFile();
            }catch( IOException e )
            {
                e.printStackTrace();
            }
        }
        */

    }

}
