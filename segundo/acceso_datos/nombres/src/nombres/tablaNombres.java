package nombres;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class tablaNombres
{
    private ArrayList<String> nombres = new ArrayList<String>();
    private String    nombreFichero;
    private File        fichero;
    private boolean existeFichero = false;
    private Scanner sc                  = new Scanner( System.in );

    public tablaNombres ()
    {
        this.nombreFichero = "fichero_" + Math.floor( Math.random() * 100 ) + ".txt";
    }

    public void insertaNombre ()
    {
        String nuevoNombre;
        System.out.println( "Nuevo Nombre:" );
        nuevoNombre = sc.nextLine();
        nombres.add( nuevoNombre );
    }

    public void crearFichero ()
    {
        System.out.println( "Nombre al fichero" );
        this.nombreFichero = sc.nextLine();
        try
        {
            fichero          = new File( nombreFichero );
            existeFichero = fichero.createNewFile();

        }catch( IOException e )
        {
            System.out.println( e.getMessage() );
        }
    }

    public void escribeFichero ()
    {
        String salida = imprimeNombres();
        if ( existeFichero )
        {
            FileWriter ficheroFW;
            try
            {
                ficheroFW = new FileWriter( fichero );
                BufferedWriter ficheroBW = new BufferedWriter( ficheroFW );

                ficheroBW.write( salida );
                ficheroBW.close();
            }catch( IOException e )
            {
                System.out.println( e.getMessage() );
            }

        }else
            System.out.println( "No existe fichero" );
    }

    public void leerFichero ()
    {
        String linea;
        int    i = 0;
        if ( existeFichero )
        {
            FileReader ficheroR;
            try
            {
                ficheroR = new FileReader( fichero );
                BufferedReader ficheroBR = new BufferedReader( ficheroR );

                linea = ficheroBR.readLine();
                while( linea != null )
                {
                    System.out.printf( "%d.- %s\n", i++, linea );
                    linea = ficheroBR.readLine();
                }
                ficheroBR.close();
            }catch( IOException e )
            {
                System.out.println( e.getMessage() );
            }

        }
    }

    public void vaciarNombres ()
    {
        nombres.removeAll( nombres );
    }

    public String imprimeNombres ()
    {
        String nombresRetorno = "";
        for ( int i = 0; i < nombres.size(); i++ )
        {
            nombresRetorno += nombres.get( i ) + "\n";
        }
        return nombresRetorno;
    }

}
