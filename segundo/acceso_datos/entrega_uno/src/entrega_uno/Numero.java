package entrega_uno;

public class Numero {
	private int numero;

	public Numero(int n) {
		this.numero = n;
	}

	public int getValor() {
		return this.numero;
	}

	public String imprimirNumero() {
		return "Numero: " + this.numero;
	}

}
