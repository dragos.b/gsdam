package entrega_uno;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;

public class Tabla {
	
	// Atributos
	private ArrayList<Numero> numeros;
	private Scanner sc = new Scanner(System.in);

	// Constructor
	public Tabla() {
		numeros = new ArrayList<Numero>();
	}

	// Metodos	
	public void pintaMenu() {
		
		// Opciones del men�
		String[] opc = {
				"Introducir N�mero",
				"Borrar N�mero",
				"Ver N�meros",
				"N�mero Alto",
				"N�mero Bajo",
				"Crear Fichero",
				"Salir" };

		System.out.printf(
				"\n\n" + "1.- %s\n" + "2.- %s\n" + "3.- %s\n" + "4.- %s\n" + "5.- %s\n" + "6.- %s\n" + "7.- %s\n\n",
				opc[0],
				opc[1],
				opc[2],
				opc[3],
				opc[4],
				opc[5],
				opc[6] );
	}
	
	public int preguntaOpcion() {
		// Retorna la opci�n seleccionada por el usuario
		System.out.printf("Selecciona Opci�n del Men� Anterior\n\n");
		return sc.nextInt();
	}

	public void introducirNumero() {
		// A�ade al ArrayList un nuevo numero del tipo Numero
		System.out.printf("Inserta un n�mero:\n\n");
		Numero buff = new Numero(sc.nextInt());
		numeros.add(buff);
	}

	public void borraNumero() {
		// Elimina un numero del ArrayList, si se indica una posici�n
		// mayor no hace nada
		int opc;
		System.out.printf("Numero a eliminar: (Indica posici�n)\n\n");

		imprimeTabla();
		opc = sc.nextInt();
		opc -= 1;
		if (opc < numeros.size())
			numeros.remove(opc);
		else
			System.out.println("La posici�n introducida " + (opc + 1) + " no existe");
	}

	public String imprimeTabla() {
		// Retorna un String con los numeros del ArrayList
		String total = "";
		System.out.printf("Imprimiendo n�meros...\n\n");
		if (numeros.size() > 0)
			for (int i = 0; i < numeros.size(); i++) {
				System.out.printf("\t%d.- %d\n", i + 1, numeros.get(i).getValor());
				total += numeros.get(i).getValor() + " ";
			}
		
		else System.out.println("No hay numeros");
		System.out.println("\n");
		return total;
	}

	public int numeroAlto() {
		// Retorna el numero mayor del ArrayList numeros
		int mayor = 0;
		for (int i = 0; i < numeros.size(); i++) {
			if (numeros.get(i).getValor() > mayor)
				mayor = numeros.get(i).getValor();
		}
		return mayor;
	}

	public int numeroBajo() {
		// Retorna el numero menor del ArrayList numeros
		int menor = numeros.get(0).getValor();
		for (int i = 0; i < numeros.size(); i++) {
			if (numeros.get(i).getValor() < menor)
				menor = numeros.get(i).getValor();
		}
		return menor;
	}
	
	public FileWriter creaFichero(String nombre_fichero) throws IOException {
		FileWriter nuevo_archivo;
		nuevo_archivo = new FileWriter(nombre_fichero);
		System.out.println("Fichero creado " + nuevo_archivo);
		return nuevo_archivo;

	}

}
